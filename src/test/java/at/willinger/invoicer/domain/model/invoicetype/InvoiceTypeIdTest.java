package at.willinger.invoicer.domain.model.invoicetype;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.utils.categories.TestCategoryTag;
import java.math.BigDecimal;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class InvoiceTypeIdTest {

  private static final String INVOICE_TYPE_ID_VALUE = "INVOICE_TYPE_1";

  private InvoiceTypeId invoiceTypeId = new InvoiceTypeId(INVOICE_TYPE_ID_VALUE);

  @Test
  public void constructor_withValueIsNull_shouldThrowIllegalArgumentException() {
    assertThrows(
        IllegalArgumentException.class,
        () -> new InvoiceTypeId(null));
  }

  @Test
  public void constructor_withValueNotValid_shouldThrowIllegalArgumentException() {
    assertThrows(
        IllegalArgumentException.class,
        () -> new InvoiceTypeId(""));
  }

  @Test
  public void constructor_shouldStoreValue() {
    assertEquals(INVOICE_TYPE_ID_VALUE, invoiceTypeId.getValue());
  }

  @Test
  public void hashCode_withSameValue_shouldReturnSameHash() {
    InvoiceTypeId otherInvoiceTypeId = new InvoiceTypeId(INVOICE_TYPE_ID_VALUE);
    assertEquals(
        invoiceTypeId.hashCode(),
        otherInvoiceTypeId.hashCode());
  }

  @Test
  public void hashCode_withOtherValue_shouldReturnFalse() {
    InvoiceTypeId otherInvoiceTypeId = new InvoiceTypeId("INVOICE_TYPE_2");
    assertNotEquals(
        invoiceTypeId.hashCode(),
        otherInvoiceTypeId.hashCode());
  }

  @Test
  public void equals_withNull_shouldReturnFalse() {
    assertFalse(invoiceTypeId.equals(null));
  }

  @Test
  public void equals_withOtherType_shouldReturnFalse() {
    Object obj = BigDecimal.ONE;
    assertFalse(invoiceTypeId.equals(obj));
  }

  @Test
  public void equals_withSameObject_shouldReturnTrue() {
    assertTrue(invoiceTypeId.equals(invoiceTypeId));
  }

  @Test
  public void equals_withSameValues_shouldReturnTrue() {
    InvoiceTypeId otherInvoiceTypeId = new InvoiceTypeId(INVOICE_TYPE_ID_VALUE);
    assertTrue(invoiceTypeId.equals(otherInvoiceTypeId));
  }

  @Test
  public void equals_withOtherValues_shouldReturnFalse() {
    InvoiceTypeId otherInvoiceTypeId = new InvoiceTypeId("INVOICE_TYPE_2");
    assertFalse(invoiceTypeId.equals(otherInvoiceTypeId));
  }

  @Test
  public void toString_shouldReturnValue() {
    assertEquals(INVOICE_TYPE_ID_VALUE, invoiceTypeId.toString());
  }

  @Test
  public void isValid_withNull_shouldReturnFalse() {
    assertFalse(InvoiceTypeId.isValid(null));
  }

  @Test
  public void isValid_withEmptyString_shouldReturnFalse() {
    assertFalse(InvoiceTypeId.isValid(""));
  }

  @Test
  public void isValid_withInvalidCharacterSlash_shouldReturnFalse() {
    assertFalse(InvoiceTypeId.isValid("\\"));
  }

  @Test
  public void isValid_withInvalidCharacterMinus_shouldReturnFalse() {
    assertFalse(InvoiceTypeId.isValid("-"));
  }

  @Test
  public void isValid_withAlphanumericCharacters_shouldReturnTrue() {
    assertTrue(InvoiceTypeId.isValid("a1B"));
  }

  @Test
  public void isValid_withPointChar_shouldReturnTrue() {
    assertTrue(InvoiceTypeId.isValid("."));
  }

  @Test
  public void isValid_withUnderlineChar_shouldReturnTrue() {
    assertTrue(InvoiceTypeId.isValid("_"));
  }

  @Test
  public void isValid_withHashChar_shouldReturnTrue() {
    assertTrue(InvoiceTypeId.isValid("#"));
  }

}
