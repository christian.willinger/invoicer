package at.willinger.invoicer.domain.model.invoice.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceStatus;
import at.willinger.invoicer.domain.model.invoice.exception.InvalidInvoiceStatusException;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import java.util.EnumSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class RenderTest {
	
	public Invoice unfilnalizedInvoice;
	public Invoice invoice;

	@BeforeEach
	public void setUp() {
		unfilnalizedInvoice = InvoiceTestEntityFactory.getNewStandardInvoice1();
		invoice = InvoiceTestEntityFactory.getFinalizedStandardInvoice1();
	}
	
	@Test
	public void withInvoiceNotFinalized_shouldThrowInvalidInvoiceStatusException() {
		InvalidInvoiceStatusException foundEx = assertThrows(
				InvalidInvoiceStatusException.class,
				() -> unfilnalizedInvoice.render());

		assertEquals("Invoice id should be mapped in exception",
				unfilnalizedInvoice.getId(),
				foundEx.getInvoiceId());
		assertEquals("Failed invoice status should be mapped in exception",
				unfilnalizedInvoice.getStatus(),
				foundEx.getActualStatus());
		assertEquals("Allowed status for operation should be mapped in exception",
				EnumSet.of(InvoiceStatus.FINALIZED),
				foundEx.getExpectedStatuses());
	}

	@Test
	public void shouldUpdateInvoiceStatus() {
		invoice.render();
		
		assertTrue(invoice.getRendered());
	}

}
