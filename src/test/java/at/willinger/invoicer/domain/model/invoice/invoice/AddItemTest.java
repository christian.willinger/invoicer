package at.willinger.invoicer.domain.model.invoice.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceStatus;
import at.willinger.invoicer.domain.model.invoice.Serial;
import at.willinger.invoicer.domain.model.invoice.exception.InvalidInvoiceStatusException;
import at.willinger.invoicer.domain.model.invoice.item.Item;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import java.math.BigDecimal;
import java.util.EnumSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class AddItemTest {
	
	public Invoice invoice;
	private Item item1;
	private Item item2;
	private Serial serial;
	
	@BeforeEach
	public void setUp() {
		invoice = InvoiceTestEntityFactory.getNewStandardInvoice1();
		item1 = InvoiceTestEntityFactory.getItem1();
		item2 = InvoiceTestEntityFactory.getItem2();
		serial = InvoiceTestEntityFactory.getSerial1();
	}
	
	@Test
	public void withItemNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> invoice.addItem(null));
	}
	
	@Test
	public void withInvoiceAlreadyFinalized_shouldThrowInvalidInvoiceStatusException() {
		invoice.finalizeWithSerial(serial);

		InvalidInvoiceStatusException foundEx = assertThrows(
				InvalidInvoiceStatusException.class,
				() -> invoice.addItem(item1));

		assertEquals("Invoice id should be mapped in exception",
				invoice.getId(),
				foundEx.getInvoiceId());
		assertEquals("Failed invoice status should be mapped in exception",
				invoice.getStatus(),
				foundEx.getActualStatus());
		assertEquals("Allowed status for operation should be mapped in exception",
				EnumSet.of(InvoiceStatus.NEW),
				foundEx.getExpectedStatuses());
	}
	
	@Test
	public void shouldAddItem() {
		invoice.addItem(item1);
		invoice.addItem(item2);
		
		assertEquals("Two items should be added after two add operations",
				2,
				invoice.getItems().size());
		assertTrue("Item1 should be in invoice itmes list",
				invoice.getItems().contains(item1));
		assertTrue("Item2 should be in invoice itmes list",
				invoice.getItems().contains(item2));
	}
	
	@Test
	public void shouldCalculateInvoiceGrosssSum() {
		invoice.addItem(item1);
		invoice.addItem(item2);
		
		BigDecimal expectedValue = item1.getGrossPrice()
				.add(item2.getGrossPrice());
		
		assertEquals("Gross sum should be gross prices of item1 + item2",
				0,
				invoice.getGrossSum().compareTo(expectedValue));
	}
	
	@Test
	public void shouldCalculateInvoiceNetSum() {
		invoice.addItem(item1);
		invoice.addItem(item2);
		
		BigDecimal expectedValue = item1.getNetPrice()
				.add(item2.getNetPrice());
		
		assertEquals("Net sum should be gross prices of item1 + item2",
				0,
				invoice.getNetSum().compareTo(expectedValue));
	}
	
	@Test
	public void shouldCalculateInvoiceTaxSum() {
		invoice.addItem(item1);
		invoice.addItem(item2);
		
		BigDecimal expectedValue = item1.getTaxPrice()
				.add(item2.getTaxPrice());
		
		assertEquals("Tax sum should be gross prices of item1 + item2",
				0,
				invoice.getTaxSum().compareTo(expectedValue));
	}

}
