package at.willinger.invoicer.domain.model.invoice.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.utils.categories.TestCategoryTag;
import java.math.BigDecimal;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class TaxTest {
	
	@Test
	public void withValueInPercentIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> Tax.ofPercent(null));
	}
	
	@Test
	public void withValueInPercentIsUnderZero_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> Tax.ofPercent(BigDecimal.valueOf(-1L)));
	}
	
	@Test
	public void shouldRoundValueToPrecission() {
		BigDecimal valueInPercent = BigDecimal.valueOf(1.333);
		Tax tax = Tax.ofPercent(valueInPercent);
		
		assertEquals("Tax should be rounded to ints precission digits",
				Tax.getPrecissionDigits(),
				tax.getValueInPercent().scale());
		assertEquals("Compare between rounded Tax value and expected value (1.33) should be zero",
				0,
				BigDecimal.valueOf(1.33).compareTo(tax.getValueInPercent()));
	}
	
	@Test
	public void calcTaxOfNetPrice_withValueIsZero_shouldReturnZero() {
		Tax tax = Tax.ofPercent(BigDecimal.ZERO);
		BigDecimal netPrice = BigDecimal.valueOf(5);
		BigDecimal taxPrice = tax.calcTaxOfNetPrice(netPrice);
		
		assertEquals("Compare between expected tax value (0) and calculated tax value should be zero, but calculated value was" + taxPrice,
				0,
				BigDecimal.ZERO.compareTo(taxPrice));
	}
	
	@Test
	public void calcTaxOfNetPrice_withNoScaleValue_shouldReturnCorrectPrice() {
		Tax tax = Tax.ofPercent(BigDecimal.valueOf(10));
		BigDecimal netPrice = BigDecimal.valueOf(100);
		BigDecimal taxPrice = tax.calcTaxOfNetPrice(netPrice);
		
		assertEquals("Compare between expected tax (10) value and calculated tax value should be zero, but calculated value was" + taxPrice,
				0,
				BigDecimal.valueOf(10).compareTo(taxPrice));
	}
	
	@Test
	public void calcTaxOfNetPrice_withScaleValue_shouldReturnCorrectPrice() {
		Tax tax = Tax.ofPercent(BigDecimal.valueOf(10));
		BigDecimal netPrice = BigDecimal.valueOf(100.33);
		BigDecimal taxPrice = tax.calcTaxOfNetPrice(netPrice);
		
		assertEquals("Compare between expected tax (10.03) value and calculated tax value should be zero, but calculated value was" + taxPrice,
				0,
				BigDecimal.valueOf(10.03).compareTo(taxPrice));
	}
	
	@Test
	public void hashCode_withSameValue_shouldReturnEqualHash() {
		Tax tax1 = Tax.ofPercent(BigDecimal.valueOf(10));
		Tax tax2 = Tax.ofPercent(BigDecimal.valueOf(10.0));

		assertEquals(tax1.hashCode(), tax2.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentValues_shouldReturnNotEqualHash() {
		Tax tax1 = Tax.ofPercent(BigDecimal.valueOf(10));
		Tax tax2 = Tax.ofPercent(BigDecimal.valueOf(10.1));

		assertNotEquals(tax1.hashCode(), tax2.hashCode());
	}
	
	@Test
	public void equals_withSameValue_shouldReturnTrue() {
		Tax tax1 = Tax.ofPercent(BigDecimal.valueOf(10));
		Tax tax2 = Tax.ofPercent(BigDecimal.valueOf(10.0));
		
		assertTrue(tax1.equals(tax2));
	}
	
	@Test
	public void equals_withSameValueAfterRounding_shouldReturnTrue() {
		Tax tax1 = Tax.ofPercent(BigDecimal.valueOf(10.001));
		Tax tax2 = Tax.ofPercent(BigDecimal.valueOf(10.0002));
		
		assertTrue(tax1.equals(tax2));
	}
	
	@Test
	public void equals_withDifferentValues_shouldReturnFalse() {
		Tax tax1 = Tax.ofPercent(BigDecimal.valueOf(10.001));
		Tax tax2 = Tax.ofPercent(BigDecimal.valueOf(10.0002));
		
		assertTrue(tax1.equals(tax2));
	}
	
	@Test
	public void equals_withDifferentObjects_shouldReturnFalse() {
		Object otherObject = BigDecimal.ZERO;
		Tax tax1 = Tax.ofPercent(BigDecimal.valueOf(10.001));

		assertFalse(tax1.equals(otherObject));
	}
	
	@Test
	public void equals_withNull_shouldReturnFalse() {
		Tax tax1 = Tax.ofPercent(BigDecimal.valueOf(10.001));
		assertFalse(tax1.equals(null));
	}
	
	@Test
	public void toString_shouldContainTaxPercentage() {
		Tax tax1 = Tax.ofPercent(BigDecimal.valueOf(10.01));
		assertTrue(tax1.toString().contains("10.01"));
	}
	
}
