package at.willinger.invoicer.domain.model.invoice.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceStatus;
import at.willinger.invoicer.domain.model.invoice.Serial;
import at.willinger.invoicer.domain.model.invoice.exception.InvalidInvoiceStatusException;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import java.time.LocalDateTime;
import java.util.EnumSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class FinalizeWithSerialTest {
	
	public Invoice invoice;
	private Serial serial;
	
	@BeforeEach
	public void setUp() {
		invoice = InvoiceTestEntityFactory.getNewStandardInvoice1();
		serial = InvoiceTestEntityFactory.getSerial1();
	}
	
	@Test
	public void withSerialIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> invoice.finalizeWithSerial(null));
	}
	
	@Test
	public void withInvoiceAlreadyFinalized_shouldThrowInvalidInvoiceStatusException() {
		invoice.finalizeWithSerial(serial);

		InvalidInvoiceStatusException foundEx = assertThrows(
				InvalidInvoiceStatusException.class,
				() -> invoice.finalizeWithSerial(serial));

		assertEquals("Invoice id should be mapped in exception",
				invoice.getId(),
				foundEx.getInvoiceId());
		assertEquals("Failed invoice status should be mapped in exception",
				invoice.getStatus(),
				foundEx.getActualStatus());
		assertEquals("Allowed status for operation should be mapped in exception",
				EnumSet.of(InvoiceStatus.NEW),
				foundEx.getExpectedStatuses());
	}
	
	@Test
	public void shouldGenerateInvoiceDate() {
		invoice.finalizeWithSerial(serial);

		assertEquals("Actual date should be set in invoice when finalized",
				LocalDateTime.now().withNano(0),
				invoice.getInvoiceDate().withNano(0));
	}
	
	@Test
	public void shouldSetSerialNumber() {
		invoice.finalizeWithSerial(serial);
		assertEquals(serial, invoice.getSerial());
	}
	
	@Test
	public void shouldUpdateInvoiceStatus() {
		invoice.finalizeWithSerial(serial);
		assertEquals(
				InvoiceStatus.FINALIZED,
				invoice.getStatus());
	}

}
