package at.willinger.invoicer.domain.model.invoicetype.invoicetype;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeStatus;
import at.willinger.invoicer.domain.model.invoicetype.SerialSequence;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class InvoiceTypeConstructorTest {
	
	private InvoiceTypeId invoiceTypeId;
	private SerialSequence serialSequence;
	
	private InvoiceType invoiceType;
	
	@BeforeEach
	public void setUp() {
		invoiceTypeId = InvoiceTypeTestEntityFactory.getInvoiceTypeId1();
		serialSequence = InvoiceTypeTestEntityFactory.getSerialSequence();
		
		invoiceType = new InvoiceType(invoiceTypeId, serialSequence); 
	}
	
	@Test
	public void constructor_withInvoiceTypeIdIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new InvoiceType(null, serialSequence));
	}
	
	@Test
	public void constructor_withSerialSequenceIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new InvoiceType(invoiceTypeId, null));
	}
	
	@Test
	public void shouldStoreValues() {
		assertEquals(invoiceTypeId, invoiceType.getId());
		assertEquals(serialSequence, invoiceType.getSerialSequence());
	}

	@Test
	public void shouldCreateTypeAsNew() {
		assertEquals("Type should be created as new since it has only a draft template",
				InvoiceTypeStatus.NEW,
				invoiceType.getStatus());
	}

}
