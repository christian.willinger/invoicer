package at.willinger.invoicer.domain.model.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import java.math.BigDecimal;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class InvoiceIdTest {
	
	private static final String INVOICE_ID_VALUE = "GROUP_1";

	private InvoiceId invoiceId = new InvoiceId(INVOICE_ID_VALUE);
	
	@Test
	public void constructor_withValueIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new InvoiceId(null));
	}
	
	@Test
	public void constructor_withValueNotValuid_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new InvoiceId("&ASDF"));
	}
	
	@Test
	public void constructor_shouldStoreValue() {
		assertEquals(INVOICE_ID_VALUE, invoiceId.getValue());
	}
	
	@Test
	public void toString_shouldReturnValue() {
		assertEquals(INVOICE_ID_VALUE, invoiceId.toString());
	}

	@Test
	public void hashCode_withSameValue_shouldReturnEqualHash() {
		InvoiceId otherInvoiceId = new InvoiceId(INVOICE_ID_VALUE);
		assertEquals(invoiceId.hashCode(), otherInvoiceId.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentValues_shouldReturnNotEqualHash() {
		InvoiceId otherInvoiceId = new InvoiceId("GROUP_2");
		assertNotEquals(invoiceId.hashCode(), otherInvoiceId.hashCode());
	}
	
	@Test
	public void equals_withNull_shouldReturnFalse() {
		assertFalse(invoiceId.equals(null));
	}
	
	@Test
	public void equals_withOtherType_shouldReturnFalse() {
		Object obj = BigDecimal.ONE;
		assertFalse(invoiceId.equals(obj));
	}
	
	@Test
	public void equals_withSameObject_shouldReturnTrue() {
		assertTrue(invoiceId.equals(invoiceId));
	}
	
	@Test
	public void equals_withSameValues_shouldReturnTrue() {
		InvoiceId otherInvoiceId = new InvoiceId(INVOICE_ID_VALUE);
		assertTrue(invoiceId.equals(otherInvoiceId));
	}
	
	@Test
	public void equals_withOtherValues_shouldReturnFalse() {
		InvoiceId otherInvoiceId = new InvoiceId("GROUP_2");
		assertFalse(invoiceId.equals(otherInvoiceId));
	}

	@Test
	public void isValid_withNull_shouldReturnFalse() {
		assertFalse(InvoiceId.isValid(null));
	}

	@Test
	public void isValid_withEmptyString_shouldReturnFalse() {
		assertFalse(InvoiceId.isValid(""));
	}

	@Test
	public void isValid_withInvalidCharacterSlash_shouldReturnFalse() {
		assertFalse(InvoiceId.isValid("\\"));
	}

	@Test
	public void isValid_withInvalidCharacterMinus_shouldReturnFalse() {
		assertFalse(InvoiceId.isValid("-"));
	}

	@Test
	public void isValid_withAlphanumericCharacters_shouldReturnTrue() {
		assertTrue(InvoiceId.isValid("a1B"));
	}

	@Test
	public void isValid_withPointChar_shouldReturnTrue() {
		assertTrue(InvoiceTypeId.isValid("."));
	}

	@Test
	public void isValid_withUnderlineChar_shouldReturnTrue() {
		assertTrue(InvoiceId.isValid("_"));
	}

	@Test
	public void isValid_withHashChar_shouldReturnTrue() {
		assertTrue(InvoiceId.isValid("#"));
	}
	
}
