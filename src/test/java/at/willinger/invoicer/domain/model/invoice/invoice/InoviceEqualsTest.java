package at.willinger.invoicer.domain.model.invoice.invoice;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import at.willinger.invoicer.domain.model.invoice.Address;
import at.willinger.invoicer.domain.model.invoice.CustomerId;
import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.types.StandardInvoice;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class InoviceEqualsTest {

	private InvoiceId invoiceId;
	private CustomerId customerId;
	private InvoiceType invoiceType;
	private Address address;
	private Invoice invoice;
	
	@BeforeEach
	public void setUp() {
		invoiceId = InvoiceTestEntityFactory.getInvoiceId1();
		customerId = InvoiceTestEntityFactory.getCustomerId1();
		invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType1();
		address = InvoiceTestEntityFactory.getAddress1();

		invoice = new StandardInvoice(invoiceId, customerId, invoiceType, address);
	}

	@Test
	public void withSameObject_shouldReturnTrue() {
		assertTrue(invoice.equals(invoice));
	}
	
	@Test
	public void withObjectHasSameId_shouldReturnTrue() {
		Invoice otherInvoice = new StandardInvoice(invoiceId, customerId, invoiceType, address);
		assertTrue(invoice.equals(otherInvoice));
	}
	
	@Test
	public void withObjectOfOtherType_shouldReturnFalse() {
		Object otherObject = BigDecimal.ONE;
		assertFalse(invoice.equals(otherObject));
	}
	
	@Test
	public void withNull_shouldReturnFalse() {
		assertFalse(invoice.equals(null));
	}
	
	@Test
	public void withObjectHasOtherId_shouldReturnFalse() {
		InvoiceId otherId = InvoiceTestEntityFactory.getInvoiceId2();
		Invoice otherInvoice = new StandardInvoice(otherId, customerId, invoiceType, address);
		assertFalse(invoice.equals(otherInvoice));
	}
	
}
