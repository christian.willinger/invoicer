package at.willinger.invoicer.domain.model.invoicetype.invoicetype;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.SerialSequence;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class InvoiceTypeEqualsTest {
	
	private InvoiceTypeId invoiceTypeId;
	private SerialSequence serialSequence;
	
	private InvoiceType invoiceType;
	
	@BeforeEach
	public void setUp() {
		invoiceTypeId = InvoiceTypeTestEntityFactory.getInvoiceTypeId1();
		serialSequence = InvoiceTypeTestEntityFactory.getSerialSequence();
		
		invoiceType = new InvoiceType(invoiceTypeId, serialSequence); 
	}
	
	@Test
	public void withSameObject_shouldReturnTrue() {
		assertTrue(invoiceType.equals(invoiceType));
	}
	
	@Test
	public void withObjectHasSameId_shouldReturnTrue() {
		InvoiceType otherInvoiceType = new InvoiceType(invoiceTypeId, serialSequence);

		assertTrue(invoiceType.equals(otherInvoiceType));
	}
	
	@Test
	public void withObjectOfOtherType_shouldReturnFalse() {
		Object otherObject = BigDecimal.ONE;

		assertFalse(invoiceType.equals(otherObject));
	}
	
	@Test
	public void withNull_shouldReturnFalse() {
		assertFalse(invoiceType.equals(null));
	}
	
	@Test
	public void withObjectHasOtherId_shouldReturnFalse() {
		InvoiceTypeId otherId = InvoiceTypeTestEntityFactory.getInvoiceTypeId2();
		InvoiceType otherInvoiceType = new InvoiceType(otherId, serialSequence);

		assertFalse(invoiceType.equals(otherInvoiceType));
	}

}
