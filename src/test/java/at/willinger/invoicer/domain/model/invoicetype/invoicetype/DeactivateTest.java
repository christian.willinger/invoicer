package at.willinger.invoicer.domain.model.invoicetype.invoicetype;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeStatus;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvalidInvoiceTypeStatusException;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.util.EnumSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class DeactivateTest {
	
	private InvoiceType invoiceType;
	private InvoiceTypeId invoiceTypeId;
	
	@BeforeEach
	public void setUp() {
		invoiceType = InvoiceTypeTestEntityFactory.getInvoiceTypeWithoutTemplates();
		invoiceTypeId = invoiceType.getId();

		invoiceType.setTemplateFileName("someRenderFile.xml");
		invoiceType.setCancellationTemplateFileName("someStornoRenderingFile.xml");
	}
	
	@Test
	public void withStatusClosed_shouldThrowInvalidInvoiceTypeStatusException() {
		invoiceType.deactivateInvoiceType();

		InvalidInvoiceTypeStatusException foundEx = assertThrows(
				InvalidInvoiceTypeStatusException.class,
				() -> invoiceType.deactivateInvoiceType());

		assertEquals("Id of invoice type should be mapped",
				invoiceTypeId,
				foundEx.getInvoiceTypeId());
		assertEquals("Actual status of invoice type should be mapped",
				InvoiceTypeStatus.INACTIVE,
				foundEx.getActualStatus());
		assertEquals("Expected statuses of invoice type should be mapped",
				EnumSet.of(InvoiceTypeStatus.NEW, InvoiceTypeStatus.ACTIVE),
				foundEx.getExpectedStatuses());
	}
	
	@Test
	public void withStatusNew_shouldChangeStatusToDeactivated() {
		invoiceType.deactivateInvoiceType();

		assertEquals("Status should be moved from new to inactive",
				InvoiceTypeStatus.INACTIVE,
				invoiceType.getStatus());
	}
	
	@Test
	public void shouldChangeStatusToDeactivated() {
		invoiceType.activate();
		
		invoiceType.deactivateInvoiceType();

		assertEquals("Status should be moved from new to inactive",
				InvoiceTypeStatus.INACTIVE,
				invoiceType.getStatus());
	}

}
