package at.willinger.invoicer.domain.model.invoicetype.invoicetype;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.SerialSequence;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class IncrementSerialTest {

	private InvoiceTypeId id;
	private SerialSequence serialSequence;
	private InvoiceType invoiceType;
	
	@BeforeEach
	public void setUp() {
		id = InvoiceTypeTestEntityFactory.getInvoiceTypeId1();
		serialSequence = InvoiceTypeTestEntityFactory.getSerialSequence();
		invoiceType = new InvoiceType(id, serialSequence);
	}
	
	@Test
	public void shouldIncrementSerialSequence() {
		long expectedValue = invoiceType.getSerialSequence().getValue() + 1;
		
		invoiceType.incrementSerial();
		
		assertEquals("Serial after increment should have value of old serial + 1",
				expectedValue,
				invoiceType.getSerialSequence().getValue().longValue());
	}

	
}
