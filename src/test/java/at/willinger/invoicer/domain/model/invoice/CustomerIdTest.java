package at.willinger.invoicer.domain.model.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.utils.categories.TestCategoryTag;
import java.math.BigDecimal;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class CustomerIdTest {
	
	private static final String CUSTOMER_ID_VALUE = "CUSTOMER_1";

	private CustomerId customerId = new CustomerId(CUSTOMER_ID_VALUE);
	
	@Test
	public void constructor_withValueIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new CustomerId(null));
	}
	
	@Test
	public void constructor_withValueIsEmpty_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new CustomerId(""));
	}
	
	@Test
	public void constructor_shouldStoreValue() {
		assertEquals(CUSTOMER_ID_VALUE, customerId.getValue());
	}
	
	@Test
	public void toString_shouldReturnValue() {
		assertEquals(CUSTOMER_ID_VALUE, customerId.toString());
	}
	
	@Test
	public void hashCode_withEqualValues_shouldReturnEqualHash() {
		CustomerId otherCustomerId = new CustomerId(CUSTOMER_ID_VALUE);
		assertEquals(customerId.hashCode(), otherCustomerId.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentValues_shouldNotReturnEqualHash() {
		CustomerId otherCustomerId = new CustomerId("CUSTOMER_2");
		assertNotEquals(customerId.hashCode(), otherCustomerId.hashCode());
	}
	
	@Test
	public void equals_withSameObject_shouldReturnTrue() {
		assertTrue(customerId.equals(customerId));
	}
	
	@Test
	public void equals_withObjectOfSameValues_shouldReturnTrue() {
		CustomerId otherCustomerId = new CustomerId(CUSTOMER_ID_VALUE);
		assertTrue(customerId.equals(otherCustomerId));
	}
	
	@Test
	public void equals_withNull_shouldReturnFalse() {
		assertFalse(customerId.equals(null));
	}
	
	@Test
	public void equals_withObjectOfOtherType_shouldReturnFalse() {
		Object otherObject = BigDecimal.ZERO;
		assertFalse(customerId.equals(otherObject));
	}
	
	@Test
	public void equals_withSerialOfDifferenValue_shouldReturnFalse() {
		CustomerId otherCustomerId = new CustomerId("CUSTOMER_2");
		assertFalse(customerId.equals(otherCustomerId));
	}

}
