package at.willinger.invoicer.domain.model.invoicetype;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Serial;
import at.willinger.invoicer.domain.model.invoicetype.exception.EndOfSerialSequenceException;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class SerialSequenceTest {

  private final static Long START = 3L;
  private final static Long END = 6L;

  private SerialSequence sequence;

  @BeforeEach
  public void setUp() {
    sequence = new SerialSequence(START, END);
  }

  @Test
  public void constructor_withStartIsNull_shouldThrowIllegalArgumentException() {
    assertThrows(
        IllegalArgumentException.class,
        () -> new SerialSequence(null, END));
  }

  @Test
  public void constructor_withEndIsNull_shouldThrowIllegalArgumentException() {
    assertThrows(
        IllegalArgumentException.class,
        () -> new SerialSequence(START, null));
  }

  @Test
  public void constructor_withStartBeforeEnd_shouldSwapValues() {
    SerialSequence otherSequence = new SerialSequence(END, START);

    assertEquals("Lower value should be used as start value",
        START,
        otherSequence.getStart());
    assertEquals("Higher value should be used as end value",
        END,
        otherSequence.getEnd());
  }

  @Test
  public void constructor_shouldStoreValues() {
    assertEquals("Start value should be stored",
        START,
        sequence.getStart());
    assertEquals("Start value should be stored",
        END,
        sequence.getEnd());
  }

  @Test
  public void constructor_shouldSetActualValueToStart() {
    assertEquals("Start value should be stored",
        START,
        sequence.getValue());
  }

  @Test
  public void increment_shouldCreateNewIncrementedSequence() {
    SerialSequence incrementedSequence = sequence.increment();

    assertEquals("Serial after increment should have value of old serial + 1",
        sequence.getValue() + 1,
        incrementedSequence.getValue().intValue());

  }

  @Test
  public void increment_withEndReached_shouldThrowEndOfSerialSequenceException() {
    EndOfSerialSequenceException foundEx = null;

    sequence = sequence.increment();
    sequence = sequence.increment();
    sequence = sequence.increment();

    try {
      sequence.increment();
    } catch(EndOfSerialSequenceException ex) {
      foundEx = ex;
    }

    assertNotNull(EndOfSerialSequenceException.class.getName(),
        foundEx);
  }

  @Test
  public void getSerial_shouldCreateSerialWithActualValue() {
    Long actualValue = sequence.getValue();

    Serial serial = sequence.getSerial();

    assertEquals("Serial is expected to have actual value",
        actualValue,
        serial.getValue());
  }

  @Test
  public void hashCode_withSameValues_shouldReturnSameHashCode() {
    SerialSequence otherSequence = new SerialSequence(START, END);

    assertEquals(
        sequence.hashCode(),
        otherSequence.hashCode());
  }

  @Test
  public void hashCode_withOtherStart_shouldReturnDiferentHashCode() {
    Long otherStart = START + 1;
    SerialSequence otherSequence = new SerialSequence(otherStart, END);

    assertNotEquals(
        sequence.hashCode(),
        otherSequence.hashCode());
  }

  @Test
  public void hashCode_withOtherEnd_shouldReturnDiferentHashCode() {
    Long otherEnd = END + 1;
    SerialSequence otherSequence = new SerialSequence(START, otherEnd);

    assertNotEquals(
        sequence.hashCode(),
        otherSequence.hashCode());
  }

  @Test
  public void hashCode_withOtherValue_shouldReturnDiferentHashCode() {
    SerialSequence otherSequence = sequence.increment();

    assertNotEquals(
        sequence.hashCode(),
        otherSequence.hashCode());
  }

  @Test
  public void equals_withNull_shouldReturnFalse() {
    assertFalse(sequence.equals(null));
  }

  @Test
  public void equals_withSameObject_shouldReturnTrue() {
    assertTrue(sequence.equals(sequence));
  }

  @Test
  public void equals_withObjectOfOtherType_shouldReturnFalse() {
    Object otherObject = BigDecimal.ONE;

    assertFalse(sequence.equals(otherObject));
  }

  @Test
  public void equals_withSameValues_shouldReturnTrue() {
    SerialSequence otherSequence = new SerialSequence(START, END);

    assertTrue(sequence.equals(otherSequence));
  }

  @Test
  public void equals_withOtherStart_shouldReturnFalse() {
    Long otherStart = START + 1;
    SerialSequence otherSequence = new SerialSequence(otherStart, END);

    assertFalse(sequence.equals(otherSequence));
  }

  @Test
  public void equals_withOtherEnd_shouldReturnFalse() {
    Long otherEnd = END + 1;
    SerialSequence otherSequence = new SerialSequence(START, otherEnd);

    assertFalse(sequence.equals(otherSequence));
  }

  @Test
  public void equals_withOtherValue_shouldReturnFalse() {
    SerialSequence otherSequence = sequence.increment();

    assertFalse(sequence.equals(otherSequence));
  }

}
