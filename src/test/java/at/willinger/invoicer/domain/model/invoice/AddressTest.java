package at.willinger.invoicer.domain.model.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.utils.categories.TestCategoryTag;
import java.math.BigDecimal;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class AddressTest {
	
	private static final String LINE_1 = "Herr";
	private static final String LINE_2 = "Max Musterman";
	private static final String LINE_3 = "Muststrasse 1 / 12";
	private static final String LINE_4 = "1200 Musterhausen";
	private static final String LINE_5 = "Musterland";
	
	private Address address;
	
	@BeforeEach
	public void setUp() {
		address = new Address(LINE_1, LINE_2, LINE_3, LINE_4, LINE_5);
	}
	
	@Test
	public void withLine1Null_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(null, LINE_2, LINE_3, LINE_4, LINE_5));
	}

	@Test
	public void withLine2Null_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, null, LINE_3, LINE_4, LINE_5));
	}

	@Test
	public void withLine3Null_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, LINE_2, null, LINE_4, LINE_5));
	}

	@Test
	public void withLine4Null_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, LINE_2, LINE_3, null, LINE_5));
	}

	@Test
	public void withLine5Null_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, LINE_2, LINE_3, LINE_4, null));
	}

	@Test
	public void withLine1ViolatesAllowedChars_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address("!", LINE_2, LINE_3, LINE_4, LINE_5));
	}

	@Test
	public void withLine2ViolatesAllowedChars_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, "$", LINE_3, LINE_4, LINE_5));
	}

	@Test
	public void withLine3ViolatesAllowedChars_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, LINE_2, "(", LINE_4, LINE_5));
	}

	@Test
	public void withLine4ViolatesAllowedChars_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, LINE_2, LINE_3, "*", LINE_5));
	}

	@Test
	public void constructor_withLine5ViolatesAllowedChars_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, LINE_2, LINE_3, LINE_4, "*"));
	}
	
	@Test
	public void constructor_withLineContainsIllegalCharacter_shouldContainLineNumberInErrorMessage() {
		IllegalArgumentException foundEx = assertThrows(
				IllegalArgumentException.class,
				() -> new Address(LINE_1, LINE_2, LINE_3, "*", LINE_5));

		assertTrue("Linenumber (4) is expected to be in error message",
				foundEx.getMessage().contains("4"));
	}
	
	@Test
	public void constructor_withLine1HasSpacesInBeginningAndEnd_shouldRemoveSpaces() {
		String spacedLine = "  " +  LINE_1 + "   ";
		Address otherAddress = new Address(spacedLine, LINE_2, LINE_3, LINE_4, LINE_5);
		assertEquals(LINE_1, otherAddress.getLine1());
	}
	
	@Test
	public void constructor_withLine2HasSpacesInBeginningAndEnd_shouldRemoveSpaces() {
		String spacedLine = "  " +  LINE_2 + "   ";
		Address otherAddress = new Address(LINE_1, spacedLine, LINE_3, LINE_4, LINE_5);
		assertEquals(LINE_2, otherAddress.getLine2());
	}
	
	@Test
	public void constructor_withLine3HasSpacesInBeginningAndEnd_shouldRemoveSpaces() {
		String spacedLine = "  " +  LINE_3 + "   ";
		Address otherAddress = new Address(LINE_1, LINE_2, spacedLine, LINE_4, LINE_5);
		assertEquals(LINE_3, otherAddress.getLine3());
	}
	
	@Test
	public void constructor_withLine4HasSpacesInBeginningAndEnd_shouldRemoveSpaces() {
		String spacedLine = "  " +  LINE_4 + "   ";
		Address otherAddress = new Address(LINE_1, LINE_2, LINE_3, spacedLine, LINE_5);
		assertEquals(LINE_4, otherAddress.getLine4());
	}
	
	@Test
	public void constructor_withLine5HasSpacesInBeginningAndEnd_shouldRemoveSpaces() {
		String spacedLine = "  " +  LINE_5 + "   ";
		Address otherAddress = new Address(LINE_1, LINE_2, LINE_3, LINE_4, spacedLine);
		assertEquals(LINE_5, otherAddress.getLine5());
	}

	@Test
	public void shouldStoreValues() {
		assertEquals(LINE_1, address.getLine1());
		assertEquals(LINE_2, address.getLine2());
		assertEquals(LINE_3, address.getLine3());
		assertEquals(LINE_4, address.getLine4());
		assertEquals(LINE_5, address.getLine5());
	}
	
	@Test
	public void hashCode_withEqualsValues_shouldReturnEqualHash() {
		Address otherAddress = new Address(LINE_1, LINE_2, LINE_3, LINE_4, LINE_5);
		assertEquals(address.hashCode(), otherAddress.hashCode());
	}
	
	@Test
	public void hashCode_withLine1Different_shouldNotReturnEqualHash() {
		String otherLine = LINE_1 + "someOtherPostfix";
		Address otherAddress = new Address(otherLine, LINE_2, LINE_3, LINE_4, LINE_5);
		assertNotEquals(address.hashCode(), otherAddress.hashCode());
	}
	
	@Test
	public void hashCode_withLine2Different_shouldNotReturnEqualHash() {
		String otherLine = LINE_2 + "someOtherPostfix";
		Address otherAddress = new Address(LINE_1, otherLine, LINE_3, LINE_4, LINE_5);
		assertNotEquals(address.hashCode(), otherAddress.hashCode());
	}
	
	@Test
	public void hashCode_withLine3Different_shouldNotReturnEqualHash() {
		String otherLine = LINE_3 + "someOtherPostfix";
		Address otherAddress = new Address(LINE_1, LINE_2, otherLine, LINE_4, LINE_5);
		assertNotEquals(address.hashCode(), otherAddress.hashCode());
	}
	
	@Test
	public void hashCode_withLine4Different_shouldNotReturnEqualHash() {
		String otherLine = LINE_4 + "someOtherPostfix";
		Address otherAddress = new Address(LINE_1, LINE_2, LINE_3, otherLine, LINE_5);
		assertNotEquals(address.hashCode(), otherAddress.hashCode());
	}
	
	@Test
	public void hashCode_withLine5Different_shouldNotReturnEqualHash() {
		String otherLine = LINE_5 + "someOtherPostfix";
		Address otherAddress = new Address(LINE_1, LINE_2, LINE_3, LINE_4, otherLine);
		assertNotEquals(address.hashCode(), otherAddress.hashCode());
	}
	
	@Test
	public void equals_withSameObject_shouldReturnTrue() {
		assertTrue(address.equals(address));
	}
	
	@Test
	public void equals_withObjectOfSameValues_shouldReturnTrue() {
		Address otherAddress = new Address(LINE_1, LINE_2, LINE_3, LINE_4, LINE_5);
		assertTrue(address.equals(otherAddress));
	}
	
	@Test
	public void equals_withNull_shouldReturnFalse() {
		assertFalse(address.equals(null));
	}
	
	@Test
	public void equals_withObjectOfOtherType_shouldReturnFalse() {
		Object otherObject = BigDecimal.ZERO;
		assertFalse(address.equals(otherObject));
	}
	
	@Test
	public void equals_withOtherAddressHasLine1Different_shouldReturnFalse() {
		String otherLine = LINE_1 + "someOtherPostfix";
		Address otherAddress = new Address(otherLine, LINE_2, LINE_3, LINE_4, LINE_5);
		assertFalse(address.equals(otherAddress));
	}
	
	@Test
	public void equals_withOtherAddressHasLine2Different_shouldReturnFalse() {
		String otherLine = LINE_2 + "someOtherPostfix";
		Address otherAddress = new Address(LINE_1, otherLine, LINE_3, LINE_4, LINE_5);
		assertFalse(address.equals(otherAddress));	
	}
	
	@Test
	public void equals_withOtherAddressHasLine3Different_shouldReturnFalse() {
		String otherLine = LINE_3 + "someOtherPostfix";
		Address otherAddress = new Address(LINE_1, LINE_2, otherLine, LINE_4, LINE_5);
		assertFalse(address.equals(otherAddress));
	}
	
	@Test
	public void equals_withOtherAddressHasLine4Different_shouldReturnFalse() {
		String otherLine = LINE_4 + "someOtherPostfix";
		Address otherAddress = new Address(LINE_1, LINE_2, LINE_3, otherLine, LINE_5);
		assertFalse(address.equals(otherAddress));
	}
	
	@Test
	public void equals_withOtherAddressHasLine5Different_shouldReturnFalse() {
		String otherLine = LINE_5 + "someOtherPostfix";
		Address otherAddress = new Address(LINE_1, LINE_2, LINE_3, LINE_4, otherLine);
		assertFalse(address.equals(otherAddress));
	}
	
	@Test
	public void toString_shouldContainAllLines() {
		assertTrue(address.toString().contains(LINE_1));
		assertTrue(address.toString().contains(LINE_2));
		assertTrue(address.toString().contains(LINE_3));
		assertTrue(address.toString().contains(LINE_4));
		assertTrue(address.toString().contains(LINE_5));
	}
	
	@Test
	public void isValidLine_withLowerCaseLetter_shouldReturnTrue() {
		assertTrue(Address.isValidLine("a"));
		assertTrue(Address.isValidLine("z"));
	}
	
	@Test
	public void isValidLine_withUpperCaseLetter_shouldReturnTrue() {
		assertTrue(Address.isValidLine("A"));
		assertTrue(Address.isValidLine("Z"));
	}	
	
	@Test
	public void isValidLine_withNumber_shouldReturnTrue() {
		assertTrue(Address.isValidLine("1"));
		assertTrue(Address.isValidLine("9"));
	}
	
	@Test
	public void isValidLine_withPoint_shouldReturnTrue() {
		assertTrue(Address.isValidLine("."));
	}
	
	@Test
	public void isValidLine_withComma_shouldReturnTrue() {
		assertTrue(Address.isValidLine(","));
	}
	
	@Test
	public void isValidLine_withApostrophe_shouldReturnTrue() {
		assertTrue(Address.isValidLine("'"));
	}
	
	@Test
	public void isValidLine_withAnd_shouldReturnTrue() {
		assertTrue(Address.isValidLine("&"));
	}
	
	@Test
	public void isValidLine_withSlash_shouldReturnTrue() {
		assertTrue(Address.isValidLine("/"));
	}
	
	@Test
	public void isValidLine_withMinus_shouldReturnTrue() {
		assertTrue(Address.isValidLine("-"));
	}
	
	@Test
	public void isValidLine_withLessThan40Characters_shouldReturnFalse() {
		String str = StringUtils.repeat("a", 39);
		assertTrue(Address.isValidLine(str));
	}
	
	@Test
	public void isValidLine_with40Characters_shouldReturnFalse() {
		String str = StringUtils.repeat("a", 40);
		assertTrue(Address.isValidLine(str));
	}
	
	@Test
	public void isValidLine_withMoreThan40Characters_shouldReturnFalse() {
		String str = StringUtils.repeat("a", 41);
		assertFalse(Address.isValidLine(str));
	}
	
}
