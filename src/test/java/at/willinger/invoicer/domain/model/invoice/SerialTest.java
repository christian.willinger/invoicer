package at.willinger.invoicer.domain.model.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import at.willinger.invoicer.utils.categories.UnitTest;
import java.math.BigDecimal;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTest.class)
public class SerialTest {
	
	private static final Long SERIAL_VALUE = 1L;

	private Serial serial = new Serial(SERIAL_VALUE);
	
	@Test(expected = IllegalArgumentException.class)
	public void constructor_withValueIsNull_shouldThrowIllegalArgumentException() {
		new Serial(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructor_withValueIsZero_shouldThrowIllegalArgumentException() {
		new Serial(0L);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructor_withValueIsNegative_shouldThrowIllegalArgumentException() {
		new Serial(-1L);
	}
	
	@Test
	public void constructor_shouldStoreValue() {
		assertEquals(SERIAL_VALUE, serial.getValue());
	}
	
	@Test
	public void hashCode_withEqualValues_shouldReturnEqualHash() {
		Serial otherSerial = new Serial(SERIAL_VALUE);
		assertEquals(serial.hashCode(), otherSerial.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentValues_shouldNotReturnEqualHash() {
		Serial otherSerial = new Serial(SERIAL_VALUE + 1);
		assertNotEquals(serial.hashCode(), otherSerial.hashCode());
	}
	
	@Test
	public void equals_withSameObject_shouldReturnTrue() {
		assertTrue(serial.equals(serial));
	}
	
	@Test
	public void equals_withObjectOfSameValues_shouldReturnTrue() {
		Serial otherSerial = new Serial(SERIAL_VALUE);
		assertTrue(serial.equals(otherSerial));
	}
	
	@Test
	public void equals_withNull_shouldReturnFalse() {
		assertFalse(serial.equals(null));
	}
	
	@Test
	public void equals_withObjectOfOtherType_shouldReturnFalse() {
		Object otherObject = BigDecimal.ZERO;
		assertFalse(serial.equals(otherObject));
	}
	
	@Test
	public void equals_withSerialOfDifferenValue_shouldReturnFalse() {
		Serial otherSerial = new Serial(SERIAL_VALUE + 1);
		assertFalse(serial.equals(otherSerial));
	}
	
	@Test
	public void toString_shouldReturnValue() {
		assertEquals(SERIAL_VALUE.toString(), serial.toString());
	}

}
