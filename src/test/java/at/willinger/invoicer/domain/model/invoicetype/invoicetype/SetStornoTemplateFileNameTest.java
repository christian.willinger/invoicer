package at.willinger.invoicer.domain.model.invoicetype.invoicetype;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.util.AssertionErrors.assertEquals;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeStatus;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvalidInvoiceTypeStatusException;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.util.EnumSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class SetStornoTemplateFileNameTest {

	private static final String FILE_NAME = "renderingFile.xml";

	private InvoiceType invoiceType;
	private InvoiceTypeId invoiceTypeId;

	@BeforeEach
	public void setUp() {
		invoiceType = InvoiceTypeTestEntityFactory.getInvoiceTypeWithoutTemplates();
		invoiceTypeId = invoiceType.getId();
	}

	@Test
	public void withNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> invoiceType.setCancellationTemplateFileName(null));
	}

	@Test
	public void withEmptyString_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> invoiceType.setCancellationTemplateFileName(""));
	}

	@Test
	public void withStatusActive_shouldThrowInvalidInvoiceTypeStatusException() {
		invoiceType.setTemplateFileName("test.jrxml");
		invoiceType.setCancellationTemplateFileName("test.jrxml");
		invoiceType.activate();

		InvalidInvoiceTypeStatusException foundEx = assertThrows(
				InvalidInvoiceTypeStatusException.class,
				() -> invoiceType.setCancellationTemplateFileName(FILE_NAME));

		assertEquals("Id of invoice type should be mapped",
				invoiceTypeId,
				foundEx.getInvoiceTypeId());
		assertEquals("Actual status of invoice type should be mapped",
				InvoiceTypeStatus.ACTIVE,
				foundEx.getActualStatus());
		assertEquals("Expected statuses of invoice type should be mapped",
				EnumSet.of(InvoiceTypeStatus.NEW),
				foundEx.getExpectedStatuses());
	}

	@Test
	public void withStatusInactive_shouldThrowInvalidInvoiceTypeStatusException() {
		invoiceType.deactivateInvoiceType();

		InvalidInvoiceTypeStatusException foundEx = assertThrows(
				InvalidInvoiceTypeStatusException.class,
				() -> invoiceType.setCancellationTemplateFileName(FILE_NAME));

		assertEquals("Id of invoice type should be mapped",
				invoiceTypeId,
				foundEx.getInvoiceTypeId());
		assertEquals("Actual status of invoice type should be mapped",
				InvoiceTypeStatus.INACTIVE,
				foundEx.getActualStatus());
		assertEquals("Expected statuses of invoice type should be mapped",
				EnumSet.of(InvoiceTypeStatus.NEW),
				foundEx.getExpectedStatuses());
	}

	@Test
	public void shouldUpdateTemplateFileName() {
		invoiceType.setCancellationTemplateFileName(FILE_NAME);

		assertEquals("Filename in draft template should be changed",
				FILE_NAME,
				invoiceType.getCancellationTemplateFileName());
	}
	
}
