package at.willinger.invoicer.domain.model.invoice.invoice.types.standard;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.InvoiceStatus;
import at.willinger.invoicer.domain.model.invoice.exception.InvalidInvoiceStatusException;
import at.willinger.invoicer.domain.model.invoice.types.StandardInvoice;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import java.util.EnumSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class CancelTest {

  private InvoiceId cancellationInvoiceId;
  private InvoiceId invoiceId;
  private StandardInvoice invoice;

  @BeforeEach
  public void setUp() {
    cancellationInvoiceId = InvoiceTestEntityFactory.getInvoiceId2();
    invoice = InvoiceTestEntityFactory.getFinalizedStandardInvoice1();
    invoiceId = invoice.getId();
  }

  @Test
  public void withInvoiceNew_shouldThrowInvalidInvoiceStatusException() {
    StandardInvoice invoice = InvoiceTestEntityFactory.getNewStandardInvoice1();

    InvalidInvoiceStatusException foundEx = assertThrows(
        InvalidInvoiceStatusException.class,
        () -> invoice.cancel(cancellationInvoiceId));

    assertEquals("Invoice id should be mapped in exception",
        invoice.getId(),
        foundEx.getInvoiceId());
    assertEquals("Failed invoice status should be mapped in exception",
        InvoiceStatus.NEW,
        foundEx.getActualStatus());
    assertEquals("Allowed status for operation should be mapped in exception",
        EnumSet.of(InvoiceStatus.FINALIZED),
        foundEx.getExpectedStatuses());
  }

  @Test
  public void withInvoiceCanceled_shouldThrowInvalidInvoiceStatusException() {
    StandardInvoice invoice = InvoiceTestEntityFactory.getFinalizedStandardInvoice1();
    invoice.cancel(cancellationInvoiceId);

    InvalidInvoiceStatusException foundEx = assertThrows(
        InvalidInvoiceStatusException.class,
        () -> invoice.cancel(cancellationInvoiceId));

    assertEquals("Invoice id should be mapped in exception",
        invoice.getId(),
        foundEx.getInvoiceId());
    assertEquals("Failed invoice status should be mapped in exception",
        InvoiceStatus.CANCELED,
        foundEx.getActualStatus());
    assertEquals("Allowed status for operation should be mapped in exception",
        EnumSet.of(InvoiceStatus.FINALIZED),
        foundEx.getExpectedStatuses());
  }

  @Test
  public void withCancellationInvoiceIdIsNull_shouldThrowIllegalArgumentException() {
    assertThrows(
        IllegalArgumentException.class,
        () -> invoice.cancel(null));
  }

  @Test
  public void withCancellationInvoiceIdIsInvoiceId_shouldThrowIllegalArgumentException() {
    assertThrows(
        IllegalArgumentException.class,
        () -> invoice.cancel(invoiceId));
  }

  @Test
  public void shouldUpdateStatus() {
    invoice.cancel(cancellationInvoiceId);

    assertEquals("Invoice should be in lifecycle status canceled",
        InvoiceStatus.CANCELED,
        invoice.getStatus());
  }

  @Test
  public void shouldSetCancellationInvoiceId() {
    invoice.cancel(cancellationInvoiceId);

    assertEquals("CancellationInvoiceId should be stored",
        cancellationInvoiceId,
        invoice.getCancellationInvoiceId());
  }

}
