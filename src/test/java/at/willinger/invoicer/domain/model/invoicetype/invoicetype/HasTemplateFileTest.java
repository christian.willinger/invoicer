package at.willinger.invoicer.domain.model.invoicetype.invoicetype;

import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class HasTemplateFileTest {

    private static final String FILE_NAME = "test.jrxml";

    private InvoiceType invoiceType;

    @BeforeEach
    public void setUp() {
        invoiceType = InvoiceTypeTestEntityFactory.getInvoiceTypeWithoutTemplates();
    }

    @Test
    public void withRenderingFile_shouldReturnTrue() {
        invoiceType.setTemplateFileName(FILE_NAME);

        assertTrue(invoiceType.hasTemplateFile());
    }

    @Test
    public void withoutRenderingFile_shouldReturnFalse() {
        assertFalse(invoiceType.hasTemplateFile());
    }

}
