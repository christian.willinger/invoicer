package at.willinger.invoicer.domain.model.invoice.invoice.types.cancellation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.event.InvoiceCreatedEvent;
import at.willinger.invoicer.domain.model.invoice.Address;
import at.willinger.invoicer.domain.model.invoice.CustomerId;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.InvoiceStatus;
import at.willinger.invoicer.domain.model.invoice.types.CancellationInvoice;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class CancellationInvoiceConstructorTest {
	
	private InvoiceId invoiceId;
	private CustomerId customerId;
	private InvoiceType invoiceType;
	private Address address;
	private InvoiceId canceledInvoiceId;
	private CancellationInvoice invoice;
	
	@BeforeEach
	public void setUp() {
		invoiceId = InvoiceTestEntityFactory.getInvoiceId1();
		customerId = InvoiceTestEntityFactory.getCustomerId1();
		invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType1();
		address = InvoiceTestEntityFactory.getAddress1();
		canceledInvoiceId = InvoiceTestEntityFactory.getInvoiceId2();

		invoice = new CancellationInvoice(
				invoiceId, customerId, invoiceType, address, canceledInvoiceId);
	}
	
	@Test
	public void withInvoiceIdIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new CancellationInvoice(null, customerId, invoiceType, address, canceledInvoiceId));
	}
	
	@Test
	public void withCustomerIdIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new CancellationInvoice(invoiceId, null, invoiceType, address, canceledInvoiceId));
	}
	
	@Test
	public void withInvoiceTypeIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new CancellationInvoice(invoiceId, customerId, null, address, canceledInvoiceId));
	}

	@Test
	public void withAddressIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new CancellationInvoice(invoiceId, customerId, invoiceType, null, canceledInvoiceId));
	}

	@Test
	public void withCanceledInvoiceIdIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new CancellationInvoice(invoiceId, customerId, invoiceType, address, null));
	}

	@Test
	public void withInvoiceIdAndCanceledInvoiceIdEqual_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new CancellationInvoice(invoiceId, customerId, invoiceType, address, invoiceId));
	}

	@Test
	public void shouldCopyValues() {
		assertEquals(invoiceId, invoice.getId());
		assertEquals(customerId, invoice.getCustomerId());
		assertEquals(invoiceType, invoice.getInvoiceType());
		assertEquals(address, invoice.getAddress());
		assertEquals(canceledInvoiceId, invoice.getCanceledInvoiceId());
	}
	
	@Test
	public void shouldCreateInvoiceWithZeroSumValues() {
		assertEquals(BigDecimal.ZERO, invoice.getGrossSum());
		assertEquals(BigDecimal.ZERO, invoice.getNetSum());
		assertEquals(BigDecimal.ZERO, invoice.getTaxSum());
	}
	
	@Test
	public void shouldCreateInvoiceInStatusNew() {
		assertEquals(InvoiceStatus.NEW, invoice.getStatus());
	}
	
	@Test
	public void shouldCreateInStatusNotRendered() {
		assertFalse(invoice.getRendered());
	}
	
	@Test
	public void shouldRaiseInvoiceCreatedEvent() {
		List<InvoiceCreatedEvent> createdEvents = invoice.getDomainEvents()
				.stream()
				.filter(e -> e instanceof InvoiceCreatedEvent)
				.map(e -> (InvoiceCreatedEvent) e)
				.collect(Collectors.toList());
		
		assertEquals("On invoice crated event should be raised",
				1,
				createdEvents.size());
		assertEquals("Event should contain correct invoice id",
				invoiceId,
				createdEvents.get(0).getInvoiceId());
	}

}
