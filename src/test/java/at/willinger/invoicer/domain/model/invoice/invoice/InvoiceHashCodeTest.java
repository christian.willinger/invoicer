package at.willinger.invoicer.domain.model.invoice.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import at.willinger.invoicer.domain.model.invoice.Address;
import at.willinger.invoicer.domain.model.invoice.CustomerId;
import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.types.StandardInvoice;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class InvoiceHashCodeTest {

	private InvoiceId invoiceId;
	private CustomerId customerId;
	private InvoiceType invoiceType;
	private Address address;
	private Invoice invoice;
	
	@BeforeEach
	public void setUp() {
		invoiceId = InvoiceTestEntityFactory.getInvoiceId1();
		customerId = InvoiceTestEntityFactory.getCustomerId1();
		invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType1();
		address = InvoiceTestEntityFactory.getAddress1();
		
		invoice = new StandardInvoice(invoiceId, customerId, invoiceType, address);
	}
	
	@Test
	public void withObjectHasSameId_shouldReturnEqualHasCodes() {
		Invoice otherInvoice = new StandardInvoice(invoiceId, customerId, invoiceType, address);
		assertEquals(invoice.hashCode(), otherInvoice.hashCode());
	}
	
	@Test
	public void withObjectHasOtherId_shouldReturnNotReturnEqualHashCodes() {
		InvoiceId otherId = InvoiceTestEntityFactory.getInvoiceId2();
		Invoice otherInvoice = new StandardInvoice(otherId, customerId, invoiceType, address);
		assertNotEquals(invoice.hashCode(), otherInvoice.hashCode());
	}
}
