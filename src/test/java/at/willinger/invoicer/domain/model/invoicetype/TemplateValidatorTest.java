package at.willinger.invoicer.domain.model.invoicetype;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class TemplateValidatorTest {
	
	private InvoiceType invoiceType;
	
	@BeforeEach
	public void setUp() {
		invoiceType = InvoiceTypeTestEntityFactory.getInvoiceTypeWithoutTemplates();
	}
	
	@Test
	public void withMissingTemplateFile_shouldAddErrorMissingTemplate() {
		Set<TypeValidationError> errors = TypeValidator.validate(invoiceType);
		
		assertTrue(errors.contains(TypeValidationError.MISSING_TEMPLATE));
	}
	
	@Test
	public void withoutMissingTemplateFile_shouldNotAddErrorMissingTemplate() {
		invoiceType.setTemplateFileName("someTemplate.xml");
		
		Set<TypeValidationError> errors = TypeValidator.validate(invoiceType);
		
		assertFalse(errors.contains(TypeValidationError.MISSING_TEMPLATE));
	}
	
	@Test
	public void withMissingStornoTemplateFile_shouldAddErrorMissingTemplate() {
		Set<TypeValidationError> errors = TypeValidator.validate(invoiceType);
		
		assertTrue(errors.contains(TypeValidationError.MISSING_STORNO_TEMPLATE));
	}
	
	@Test
	public void withoutMissingStornoTemplateFile_shouldNotAddErrorMissingTemplate() {
		invoiceType.setCancellationTemplateFileName("someTemplate.xml");
		
		Set<TypeValidationError> errors = TypeValidator.validate(invoiceType);
		
		assertFalse(errors.contains(TypeValidationError.MISSING_STORNO_TEMPLATE));
	}

}
