package at.willinger.invoicer.domain.model.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.item.Item;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeRepository;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdNotFoundExcpeption;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@Tag(TestCategoryTag.MODEL)
public class InvoiceFactoryTest {
	
	private InvoiceTypeRepository invoiceTypeRepository;
	private InvoiceFactory invoiceFactory;
	
	private InvoiceTypeId invoiceTypeId;
	private InvoiceType invoiceType;
	
	private InvoiceId invoiceId;
	private CustomerId customerId;
	private Address address;
	private Item item1;
	private Item item2;
	private List<Item> items;
	
	@BeforeEach
	public void setUp() {
		invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType1();
		invoiceTypeId = invoiceType.getId();
		
		invoiceId = InvoiceTestEntityFactory.getInvoiceId1();
		customerId = InvoiceTestEntityFactory.getCustomerId1();
		address = InvoiceTestEntityFactory.getAddress1();
		
		item1 = InvoiceTestEntityFactory.getItem1();
		item2 = InvoiceTestEntityFactory.getItem2();
		items = Arrays.asList(item1, item2);
		
		invoiceTypeRepository = Mockito.mock(InvoiceTypeRepository.class);
		Mockito.when(invoiceTypeRepository.findById(invoiceTypeId))
			.thenReturn(Optional.of(invoiceType));
		
		invoiceFactory = new InvoiceFactory(invoiceTypeRepository);
	}
	
	@Test
	public void shouldLookupInvoiceType() {
		invoiceFactory.create(invoiceTypeId, invoiceId, customerId, address, items);
		
		Mockito.verify(invoiceTypeRepository, Mockito.times(1))
			.findById(invoiceTypeId);
	}
	
	@Test
	public void withNotExistingInvoiceType_shouldThrowInvoiceTypeIdNotFoundExcpeption() {
		Mockito.when(invoiceTypeRepository.findById(invoiceTypeId))
			.thenReturn(Optional.empty());

		InvoiceTypeIdNotFoundExcpeption foundEx = assertThrows(
				InvoiceTypeIdNotFoundExcpeption.class,
				() -> invoiceFactory.create(invoiceTypeId, invoiceId, customerId, address, items));

		assertEquals("Invalid Id should be mapped in exception",
				invoiceTypeId,
				foundEx.getInvoiceTypeId());
	}
	
	@Test
	public void shouldCreateInvoiceWithLoadedInvoiceType() {
		Invoice invoice = invoiceFactory.create(invoiceTypeId, invoiceId, customerId, address, items);

		assertEquals(
				invoiceType,
				invoice.getInvoiceType());
	}
	
	@Test
	public void shouldCreateInvoiceWithGivenInvoiceId() {
		Invoice invoice = invoiceFactory.create(invoiceTypeId, invoiceId, customerId, address, items);
		
		assertEquals(
				invoiceId,
				invoice.getId());
	}
	
	@Test
	public void shouldCreateInvoiceWithGivenAddress() {
		Invoice invoice = invoiceFactory.create(invoiceTypeId, invoiceId, customerId, address, items);
		
		assertEquals(
				address,
				invoice.getAddress());
	}
	
	@Test
	public void shouldAddItems() {
		Invoice invoice = invoiceFactory.create(invoiceTypeId, invoiceId, customerId, address, items);
		
		assertEquals("Item1 should be added on first position",
				item1,
				invoice.getItems().get(0));
		assertEquals("Item2 should be added on second position",
				item2,
				invoice.getItems().get(1));
	}

}
