package at.willinger.invoicer.domain.model.invoice.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.utils.categories.TestCategoryTag;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class ItemTest {
	
	private static final String DESCRIPTION = "Test item";
	private static final Unit CHARGED_UNIT = Unit.AMOUNT;
	private static final BigInteger CHARGED_AMOUNT = BigInteger.valueOf(1);
	private static final Tax TAX = Tax.ofPercent(BigDecimal.valueOf(20));
	private static final BigDecimal NET_PRICE = BigDecimal.valueOf(12.39);
	
	private Item item;
	
	@BeforeEach
	public void setUp() {
		item = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, NET_PRICE);
	}
	
	@Test
	public void constructor_withDescriptionIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Item(null, CHARGED_UNIT, CHARGED_AMOUNT, TAX, NET_PRICE));
	}

	@Test
	public void constructor_withDescriptionIsEmpty_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Item("", CHARGED_UNIT, CHARGED_AMOUNT, TAX, NET_PRICE));
	}

	@Test
	public void constructor_withChargeUnitIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Item(DESCRIPTION, null, CHARGED_AMOUNT, TAX, NET_PRICE));
	}

	@Test
	public void constructor_withChargeAmountIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Item(DESCRIPTION, CHARGED_UNIT, null, TAX, NET_PRICE));
	}

	@Test
	public void constructor_withChargeAmountIsNegative_shouldThrowIllegalArgumentException() {
		BigInteger negativeAmount = BigInteger.valueOf(-1);

		assertThrows(
				IllegalArgumentException.class,
				() -> new Item(DESCRIPTION, CHARGED_UNIT, negativeAmount, TAX, NET_PRICE));
	}

	@Test
	public void constructor_withChargeAmountIsZero_shouldThrowIllegalArgumentException() {
		BigInteger negativeAmount = BigInteger.valueOf(0);

		assertThrows(
				IllegalArgumentException.class,
				() -> new Item(DESCRIPTION, CHARGED_UNIT, negativeAmount, TAX, NET_PRICE));
	}

	@Test
	public void constructor_withTaxIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, null, NET_PRICE));
	}

	@Test
	public void constructor_withNetPriceIsNull_shouldThrowIllegalArgumentException() {
		assertThrows(
				IllegalArgumentException.class,
				() -> new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, null));
	}

	@Test
	public void constructor_shouldStoreValues() {
		assertEquals(DESCRIPTION, item.getDescription());
		assertEquals(CHARGED_UNIT, item.getChargedUnit());
		assertEquals(CHARGED_AMOUNT, item.getChargedAmount());
		assertEquals(TAX, item.getTax());
		assertEquals(NET_PRICE, item.getNetPrice());
	}
	
	@Test
	public void constructor_shouldCalculateTaxPrice() {
		BigDecimal calculatedValue = item.getTaxPrice();
		assertEquals("(12.39 / 100) * 20 should be calced to 2.478 rounded up to 2.48 but was " + calculatedValue,
				0,
				BigDecimal.valueOf(2.48).compareTo(calculatedValue));
	}
	
	@Test
	public void constructor_shouldCalculateGrossPrice() {
		BigDecimal calculatedValue = item.getGrossPrice();
		assertEquals("12.39 + (12.39 / 100) * 20 should be calced to 14,868 rounded up to 14.87 but was" + calculatedValue,
				0,
				BigDecimal.valueOf(14.87).compareTo(calculatedValue));
	}
	
	@Test
	public void shouldRoundNetPriceToMathContext() {
		BigDecimal netPrice = BigDecimal.valueOf(12.395);
		Item item = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, netPrice);
		
		BigDecimal roundedNetPrice = item.getNetPrice();
		
		assertEquals("12.395 rounded should be 12.39 but was" + roundedNetPrice,
				0,
				BigDecimal.valueOf(12.39).compareTo(roundedNetPrice));
	}
	
	@Test
	public void shouldRoundGrossPriceToMathContext() {
		BigDecimal netPrice = BigDecimal.valueOf(12.395);
		Item item = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, netPrice);
		
		BigDecimal roundedGrossPrice = item.getGrossPrice();
		
		assertEquals("12.395 + (12.395 / 100) * 20 = 14,874 rounded to 14.87" + roundedGrossPrice,
				0,
				BigDecimal.valueOf(14.87).compareTo(roundedGrossPrice));
	}
	
	@Test
	public void shouldRoundTaxPriceToMathContext() {
		BigDecimal netPrice = BigDecimal.valueOf(12.395);
		Item item = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, netPrice);
		
		BigDecimal roundedTaxPrice = item.getTaxPrice();
		
		assertEquals("(12.395 / 100) * 20 should be  2,479 rounded up to 2.48 but was" + roundedTaxPrice,
				0,
				BigDecimal.valueOf(2.48).compareTo(roundedTaxPrice));
	}
	
	@Test
	public void shouldCalculateConsistentSumAfterRounding() {
		BigDecimal netPrice = BigDecimal.valueOf(12.395);
		Item item = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, netPrice);
		
		BigDecimal invoicedNetPrice = item.getNetPrice();
		BigDecimal invoicedGrossPrice = item.getGrossPrice();
		BigDecimal invoicedTaxPrice = item.getTaxPrice();
		
		BigDecimal invoicedExpectedGrossPrice = invoicedNetPrice.add(invoicedTaxPrice);
		
		assertEquals("Grossprice " + invoicedGrossPrice + " was expected to be netPrice " + invoicedNetPrice + " + tax " + invoicedTaxPrice ,
				0,
				invoicedGrossPrice.compareTo(invoicedExpectedGrossPrice));
	}
	
	@Test
	public void hashCode_withSameValues_shouldReturnEqualHash() {
		Item otherItem = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, NET_PRICE);
		
		assertEquals(item.hashCode(), otherItem.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentDescription_shouldReturnNonEqualHash() {
		String otherDescription = DESCRIPTION + "a";
		Item otherItem = new Item(otherDescription, CHARGED_UNIT, CHARGED_AMOUNT, TAX, NET_PRICE);
		
		assertNotEquals(item.hashCode(), otherItem.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentChargedUnit_shouldReturnNonEqualHash() {
		Unit otherUnit = Unit.SECONDS;
		Item otherItem = new Item(DESCRIPTION, otherUnit, CHARGED_AMOUNT, TAX, NET_PRICE);
		
		assertNotEquals(item.hashCode(), otherItem.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentChargedAmount_shouldReturnNonEqualHash() {
		BigInteger otherChargedAmount = CHARGED_AMOUNT.add(BigInteger.ONE);
		Item otherItem = new Item(DESCRIPTION, CHARGED_UNIT, otherChargedAmount, TAX, NET_PRICE);
		
		assertNotEquals(item.hashCode(), otherItem.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentTax_shouldReturnNonEqualHash() {
		BigDecimal otherTaxValue = TAX.getValueInPercent().add(BigDecimal.ONE);
		Tax otherTax = Tax.ofPercent(otherTaxValue);
		Item otherItem = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, otherTax, NET_PRICE);
		
		assertNotEquals(item.hashCode(), otherItem.hashCode());
	}
	
	@Test
	public void hashCode_withDifferentNetPrice_shouldReturnNonEqualHash() {
		BigDecimal otherNetPrice = NET_PRICE.add(BigDecimal.ONE);
		Item otherItem = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, otherNetPrice);
		
		assertNotEquals(item.hashCode(), otherItem.hashCode());
	}
	
	@Test
	public void equals_withSameObject_shouldReturnTrue() {
		assertTrue(item.equals(item));
	}
	
	@Test
	public void equals_withObjectOfSameValues_shouldReturnTrue() {
		Item otherItem = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, NET_PRICE);
		
		assertTrue(item.equals(otherItem));
	}
	
	@Test
	public void equals_withObjectNotOfSameType_shouldReturnFalse() {
		Object object = BigDecimal.ONE;
		
		assertFalse(item.equals(object));
	}
	
	@Test
	public void equals_withNull_shouldReturnFalse() {
		assertFalse(item.equals(null));
	}
	
	@Test
	public void equals_withDifferentDescription_shouldReturnFalse() {
		String otherDescription = DESCRIPTION + "a";
		Item otherItem = new Item(otherDescription, CHARGED_UNIT, CHARGED_AMOUNT, TAX, NET_PRICE);
		
		assertFalse(item.equals(otherItem));
	}
	
	@Test
	public void equals_withDifferentChargedUnit_shouldReturnFalse() {
		Unit otherUnit = Unit.SECONDS;
		Item otherItem = new Item(DESCRIPTION, otherUnit, CHARGED_AMOUNT, TAX, NET_PRICE);
		
		assertFalse(item.equals(otherItem));
	}
	
	@Test
	public void equals_withDifferentChargedAmount_shouldReturnFalse() {
		BigInteger otherChargedAmount = CHARGED_AMOUNT.add(BigInteger.ONE);
		Item otherItem = new Item(DESCRIPTION, CHARGED_UNIT, otherChargedAmount, TAX, NET_PRICE);
		
		assertFalse(item.equals(otherItem));
	}
	
	@Test
	public void equals_withDifferentTax_shouldReturnFalse() {
		BigDecimal otherTaxValue = TAX.getValueInPercent().add(BigDecimal.ONE);
		Tax otherTax = Tax.ofPercent(otherTaxValue);
		Item otherItem = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, otherTax, NET_PRICE);
		
		assertFalse(item.equals(otherItem));
	}
	
	@Test
	public void equals_withDifferentNetPrice_shouldReturnFalse() {
		BigDecimal otherNetPrice = NET_PRICE.add(BigDecimal.ONE);
		Item otherItem = new Item(DESCRIPTION, CHARGED_UNIT, CHARGED_AMOUNT, TAX, otherNetPrice);
		
		assertFalse(item.equals(otherItem));
	}
	
}
