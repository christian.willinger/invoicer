package at.willinger.invoicer.domain.model.invoicetype.invoicetype;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.SerialSequence;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.MODEL)
public class InvoiceTypeHashCodeTest {
	
	private InvoiceTypeId invoiceTypeId;
	private SerialSequence serialSequence;
	
	private InvoiceType invoiceType;
	
	@BeforeEach
	public void setUp() {
		invoiceTypeId = InvoiceTypeTestEntityFactory.getInvoiceTypeId1();
		serialSequence = InvoiceTypeTestEntityFactory.getSerialSequence();
		
		invoiceType = new InvoiceType(invoiceTypeId, serialSequence); 
	}
	
	@Test
	public void withObjectHasSameId_shouldReturnSameHashCode() {
		InvoiceType otherInvoiceType = new InvoiceType(invoiceTypeId, serialSequence);

		assertEquals(invoiceType.hashCode(), otherInvoiceType.hashCode());
	}
	
	@Test
	public void withObjectHasOtherId_shouldNotReturnSameHashCode() {
		InvoiceTypeId otherId = InvoiceTypeTestEntityFactory.getInvoiceTypeId2();
		InvoiceType otherInvoiceType = new InvoiceType(otherId, serialSequence);

		assertNotEquals(invoiceType.hashCode(), otherInvoiceType.hashCode());
	}

}
