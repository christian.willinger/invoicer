package at.willinger.invoicer.infrastructure.storage;

import static org.junit.Assert.assertEquals;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.service.FileStorageService;
import at.willinger.invoicer.infrastructure.spring.applicationconfig.InvoiceConfig;
import at.willinger.invoicer.infrastructure.spring.applicationconfig.InvoiceTypeConfig;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InvoiceTypeFileStorageTest {
	
	protected static final String FILE_DIRECTORY = "target/testfiles1";

	private static final String CONTENT = "Some test content";
	
	private FileStorageService fileStorageService;
	
	private InvoiceType invoiceType;
	private Invoice invoice;
	
	@Before
	public void setUp() {
		InvoiceTypeConfig invoiceTypeConfig = new InvoiceTypeConfig();
		invoiceTypeConfig.setTemplateFileDirectory(FILE_DIRECTORY);
		
		InvoiceConfig invoiceConfig = new InvoiceConfig();
		invoiceConfig.setDocumentFileDirectory(FILE_DIRECTORY);
		
		LocalFilesystemFileStorageService localFilesystemFileStorageService = new LocalFilesystemFileStorageService();
		localFilesystemFileStorageService.setInvoiceTypeConfig(invoiceTypeConfig);
		localFilesystemFileStorageService.setInvoiceConfig(invoiceConfig);
		fileStorageService = localFilesystemFileStorageService;

		invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType1();
		
		invoice = InvoiceTestEntityFactory.getFinalizedStandardInvoice1();
	}
	
	@After
	public void tearDown() throws IOException {
		File folder = FileSystems.getDefault()
				.getPath(FILE_DIRECTORY)
				.toFile();
		
		org.apache.commons.io.FileUtils.deleteDirectory(folder);
	}
	
	@Test
	public void storeInvoiceTemplate_loadInvoiceTemplate_shouldStoreAndLoadTemplate() throws IOException {
		InputStream dataToStore = getContentInputStream();
		fileStorageService.storeInvoiceStornoTemplate(invoiceType, dataToStore);
		
		InputStream loadedData = fileStorageService.loadInvoiceStornoTemplate(invoiceType);
		String loadedDataString = IOUtils.toString(loadedData, StandardCharsets.UTF_8.name());
		FileUtils.closeInputStream(loadedData);
	
		assertEquals("Stored file content should be loaded",
				CONTENT,
				loadedDataString);
	}
	
	@Test(expected = IllegalStateException.class)
	public void loadInvoiceTemplate_withNotExistingFile_shouldThrowIllegalStateException() {
		fileStorageService.loadInvoiceTemplate(invoiceType);
	}
	
	@Test
	public void storeInvoiceStornoTemplate_loadInvoiceStornoTemplate_shouldStoreAndLoadTemplate() throws IOException {
		InputStream dataToStore = getContentInputStream();
		fileStorageService.storeInvoiceTemplate(invoiceType, dataToStore);
		
		InputStream loadedData = fileStorageService.loadInvoiceTemplate(invoiceType);
		String loadedDataString = IOUtils.toString(loadedData, StandardCharsets.UTF_8.name());
		FileUtils.closeInputStream(loadedData);
	
		assertEquals("Stored file content should be loaded",
				CONTENT,
				loadedDataString);
	}
	
	@Test(expected = IllegalStateException.class)
	public void loadInvoiceStornoTemplate_withNotExistingStornoTemplateFile_shouldThrowIllegalStateException() {
		fileStorageService.loadInvoiceStornoTemplate(invoiceType);
	}
	
	@Test
	public void storeInvoiceFile_loadInvoiceFile_shouldStoreAndLoadTemplate() throws IOException {
		InputStream dataToStore = getContentInputStream();
		fileStorageService.storeInvoiceFile(invoice, dataToStore);
		
		InputStream loadedData = fileStorageService.loadInvoiceFile(invoice);
		String loadedDataString = IOUtils.toString(loadedData, StandardCharsets.UTF_8.name());
		FileUtils.closeInputStream(loadedData);
		
		assertEquals("Stored file content should be loaded",
				CONTENT,
				loadedDataString);
	}
	
	@Test(expected = IllegalStateException.class)
	public void loadInvoiceFile_withNotExistingInvoiceFile_shouldThrowIllegalStateException() {
		fileStorageService.loadInvoiceFile(invoice);
	}

	private InputStream getContentInputStream() {
		return new ByteArrayInputStream(CONTENT.getBytes());
	}
	
}
