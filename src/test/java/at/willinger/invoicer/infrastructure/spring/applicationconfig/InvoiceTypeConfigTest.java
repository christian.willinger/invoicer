package at.willinger.invoicer.infrastructure.spring.applicationconfig;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InvoiceTypeConfigTest {

  protected static final String EXISTING_DIRECTORY = "target";
  protected static final String NOT_EXISTING_DIRECTORY = "target1";

  private InvoiceTypeConfig config;

  @BeforeEach
  public void setUp() {
    config = new InvoiceTypeConfig();
    config.setTemplateFileDirectory(EXISTING_DIRECTORY);
  }

  @Test
  public void init_withExistingDocumentFileDirectory_shouldPass() {
    config.setTemplateFileDirectory(EXISTING_DIRECTORY);

    config.init();
  }


  @Test
  public void init_withoutExistingDocumentFileDirectory_shouldThrowIllegalStateException() {
    config.setTemplateFileDirectory(NOT_EXISTING_DIRECTORY);

    assertThrows(
        IllegalStateException.class,
        () -> config.init());
  }

}
