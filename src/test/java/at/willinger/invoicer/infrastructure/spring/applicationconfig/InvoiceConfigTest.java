package at.willinger.invoicer.infrastructure.spring.applicationconfig;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InvoiceConfigTest {

  protected static final String EXISTING_DIRECTORY = "target";
  protected static final String NOT_EXISTING_DIRECTORY = "target1";

  private InvoiceConfig config;

  @BeforeEach
  public void setUp() {
    config = new InvoiceConfig();
    config.setDocumentFileDirectory(EXISTING_DIRECTORY);
  }

  @Test
  public void init_withExistingDocumentFileDirectory_shouldPass() {
    config.setDocumentFileDirectory(EXISTING_DIRECTORY);

    config.init();
  }


  @Test
  public void init_withoutExistingDocumentFileDirectory_shouldThrowIllegalStateException() {
    config.setDocumentFileDirectory(NOT_EXISTING_DIRECTORY);

    assertThrows(
        IllegalStateException.class,
        () -> config.init());
  }

}
