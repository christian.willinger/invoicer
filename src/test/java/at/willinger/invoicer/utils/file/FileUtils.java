package at.willinger.invoicer.utils.file;

import java.io.InputStream;

public class FileUtils {
	
	public static InputStream loadFile(String location) {
		InputStream is = ClassLoader.getSystemResourceAsStream(location);
		
		if(is == null) {
			String msg = String.format("File at location [%s] not found", location);
			throw new IllegalStateException(msg);
		}
		
		return is;
	}

}
