package at.willinger.invoicer.utils.factory.entity;

import at.willinger.invoicer.domain.model.invoice.Address;
import at.willinger.invoicer.domain.model.invoice.CustomerId;
import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.Serial;
import at.willinger.invoicer.domain.model.invoice.item.Item;
import at.willinger.invoicer.domain.model.invoice.item.Tax;
import at.willinger.invoicer.domain.model.invoice.item.Unit;
import at.willinger.invoicer.domain.model.invoice.types.StandardInvoice;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import java.math.BigDecimal;
import java.math.BigInteger;

public class InvoiceTestEntityFactory {

  public static InvoiceId getInvoiceId1() {
    return new InvoiceId("INVOICE_1");
  }

  public static InvoiceId getInvoiceId2() {
    return new InvoiceId("INVOICE_2");
  }

  public static CustomerId getCustomerId1() {
    return new CustomerId("CUSTOMER_1");
  }

  public static CustomerId getCustomerId2() {
    return new CustomerId("CUSTOMER_2");
  }

  public static Serial getSerial1() {
    return new Serial(100L);
  }

  public static Serial getSerial2() {
    return new Serial(101L);
  }

  public static Address getAddress1() {
    return new Address("Herr", "Max Mustermann", "Musterstrasse 15", "1111 Musterhausen",
        "Musterland");
  }

  public static Address getAddress2() {
    return new Address("Frau", "Maximilia Musterfrau", "Musterstrasse 15", "1111 Musterhausen",
        "Musterland");
  }

  public static Item getItem1() {
    return new Item(
        "Test item 1",
        Unit.AMOUNT,
        BigInteger.valueOf(1),
        Tax.ofPercent(BigDecimal.valueOf(20)),
        BigDecimal.valueOf(10));
  }

  public static Item getItem2() {
    return new Item(
        "Test item 2",
        Unit.AMOUNT,
        BigInteger.valueOf(1),
        Tax.ofPercent(BigDecimal.valueOf(20)),
        BigDecimal.valueOf(10));
  }

  public static StandardInvoice getNewStandardInvoice1() {
    InvoiceId id = getInvoiceId1();
    CustomerId customerId = getCustomerId1();
    InvoiceType invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType1();
    Address address = getAddress1();

    StandardInvoice invoice = new StandardInvoice(id, customerId, invoiceType, address);
    invoice.getDomainEvents().clear();

    return invoice;
  }

  public static Invoice getNewStandardInvoice2() {
    InvoiceId id = getInvoiceId2();
    CustomerId customerId = getCustomerId2();
    InvoiceType invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType2();
    Address address = getAddress2();

    StandardInvoice invoice = new StandardInvoice(id, customerId, invoiceType, address);
    invoice.getDomainEvents().clear();

    return invoice;
  }

  public static StandardInvoice getFinalizedStandardInvoice1() {
    StandardInvoice invoice = getNewStandardInvoice1();
    invoice.finalizeWithSerial(getSerial1());
    return invoice;
  }

}
