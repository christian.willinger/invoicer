package at.willinger.invoicer.utils.factory.entity;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.SerialSequence;
import at.willinger.invoicer.utils.file.FileUtils;
import java.io.InputStream;

public class InvoiceTypeTestEntityFactory {

  public static InvoiceType getInvoiceTypeWithoutTemplates() {
    InvoiceTypeId id = getInvoiceTypeId1();
    SerialSequence serial = getSerialSequence();

    return new InvoiceType(id, serial);
  }

  public static InvoiceType getActiveInvoiceType1() {
    InvoiceTypeId id = getInvoiceTypeId1();
    SerialSequence serial = getSerialSequence();

    InvoiceType type = new InvoiceType(id, serial);
    type.setTemplateFileName("template.jrmll");
    type.setCancellationTemplateFileName("stornoTemplate.jrxml");
    type.activate();

    return type;
  }

  public static InvoiceType getActiveInvoiceType2() {
    InvoiceTypeId id = getInvoiceTypeId2();
    SerialSequence serial = getSerialSequence();

    InvoiceType type = new InvoiceType(id, serial);
    type.setTemplateFileName("template.jrmll");
    type.setCancellationTemplateFileName("stornoTemplate.jrxml");
    type.activate();

    return type;
  }

  public static InvoiceTypeId getInvoiceTypeId1() {
    return new InvoiceTypeId("INVOICE_TYPE_1");
  }

  public static InvoiceTypeId getInvoiceTypeId2() {
    return new InvoiceTypeId("INVOICE_TYPE_2");
  }

  public static SerialSequence getSerialSequence() {
    return new SerialSequence(10000L, 99999L);
  }

  public static InputStream getTemplateFileInputStream() {
    return FileUtils.loadFile("template/TestInvoice_A4.jrxml");
  }

}
