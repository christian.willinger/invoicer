package at.willinger.invoicer.utils.repository;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class InvoiceTypeInMemoryRepository implements InvoiceTypeRepository {
	
	private List<InvoiceType> storage = new ArrayList<>();

	@Override
	public void persist(InvoiceType type) {
		if(!storage.contains(type)) {
			storage.add(type);
		}
	}

	@Override
	public Optional<InvoiceType> findById(InvoiceTypeId id) {
		return storage.stream()
				.filter(t -> t.getId().equals(id))
				.findAny();
	}

	@Override
	public Optional<InvoiceType> findAndLockById(InvoiceTypeId id) {
		return storage.stream()
				.filter(t -> t.getId().equals(id))
				.findAny();
	}

	@Override
	public List<InvoiceType> search() {
		return Collections.unmodifiableList(storage);
	}

	public void reset() {
		storage = new ArrayList<>();
	}

}
