package at.willinger.invoicer.utils.repository;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.InvoiceRepository;
import at.willinger.invoicer.domain.model.invoice.InvoiceSearchSpecification;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public class InvoiceInMemoryRepository implements InvoiceRepository {
	
	private List<Invoice> storage = new ArrayList<>();

	@Override
	public Invoice persist(Invoice invoice) {
		if(!storage.contains(invoice)) {
			storage.add(invoice);
		}
		
		return invoice;
	}

	@Override
	public Optional<Invoice> findById(InvoiceId id) {
		return storage.stream()
				.filter(i -> i.getId().equals(id))
				.findAny();
	}

	@Override
	public Optional<Invoice> findAndLockById(InvoiceId id) {
		return storage.stream()
				.filter(i -> i.getId().equals(id))
				.findAny();
	}

	@Override
	public Page<Invoice> findAll(InvoiceSearchSpecification spec, PageRequest pageRequest) {
		throw new UnsupportedOperationException();
	}

}
