package at.willinger.invoicer.application.invoicetypeservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdConflictException;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class CreateTest extends AbstractInvoiceTypeServiceTest {
	
	private InvoiceTypeId invoiceTypeId;
	private InvoiceType invoiceType;
	
	@BeforeEach
	public void setUp() {
		invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType1();
		invoiceTypeId = invoiceType.getId();
	}

	@Test
	public void withInvoiceIdAlreadyUsed_shouldThrowInvoiceTypeIdConflictException() {
		invoiceTypeService.create(invoiceType);

		InvoiceTypeIdConflictException foundEx = assertThrows(
				InvoiceTypeIdConflictException.class,
				() -> invoiceTypeService.create(invoiceType));

		assertEquals("Conflicting invoice id should be set in exception",
				invoiceTypeId,
				foundEx.getInvoiceTypeId());
	}
	
	@Test
	public void shouldPersistInvoiceType() {
		invoiceTypeService.create(invoiceType);
		
		Optional<InvoiceType> result = invoiceTypeRepository.findById(invoiceTypeId);
		
		assertTrue("InvoiceType should be persisted after creation",
				result.isPresent());
	}
	
}
