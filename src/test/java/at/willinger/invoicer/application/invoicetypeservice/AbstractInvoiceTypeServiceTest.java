package at.willinger.invoicer.application.invoicetypeservice;

import at.willinger.invoicer.application.InvoiceTypeService;
import at.willinger.invoicer.application.impl.InvoiceTypeServiceImpl;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeRepository;
import at.willinger.invoicer.domain.service.FileStorageService;
import at.willinger.invoicer.infrastructure.spring.applicationconfig.InvoiceTypeConfig;
import at.willinger.invoicer.infrastructure.storage.LocalFilesystemFileStorageService;
import at.willinger.invoicer.utils.repository.InvoiceTypeInMemoryRepository;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

public abstract class AbstractInvoiceTypeServiceTest {
	
	protected static final String TEMPLATE_FILE_DIRECTORY = "target/teststorage";
	
	protected InvoiceTypeRepository invoiceTypeRepository;
	protected InvoiceTypeService invoiceTypeService;
	protected FileStorageService fileStorageService;
	
	@BeforeEach
	public void setUpInfrastructure() {
		invoiceTypeRepository = new InvoiceTypeInMemoryRepository();
		
		InvoiceTypeConfig invoiceTypeConfig = new InvoiceTypeConfig();
		invoiceTypeConfig.setTemplateFileDirectory(TEMPLATE_FILE_DIRECTORY);
		
		LocalFilesystemFileStorageService localFilesystemFileStorageService = new LocalFilesystemFileStorageService();
		localFilesystemFileStorageService.setInvoiceTypeConfig(invoiceTypeConfig);
		fileStorageService = localFilesystemFileStorageService;
		
		InvoiceTypeServiceImpl invoiceTypeServiceImpl = new InvoiceTypeServiceImpl();
		invoiceTypeServiceImpl.setInvoiceTypeRepository(invoiceTypeRepository);
		invoiceTypeServiceImpl.setFileStorageService(fileStorageService);
		invoiceTypeService = invoiceTypeServiceImpl;
	}
	
	@AfterEach
	public void tearDownInfrastructure() throws IOException {
		File folder = FileSystems.getDefault()
				.getPath(TEMPLATE_FILE_DIRECTORY)
				.toFile();
		
		org.apache.commons.io.FileUtils.deleteDirectory(folder);
	}

}
