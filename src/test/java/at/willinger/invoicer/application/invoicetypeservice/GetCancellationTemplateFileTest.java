package at.willinger.invoicer.application.invoicetypeservice;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdNotFoundExcpeption;
import at.willinger.invoicer.domain.model.invoicetype.exception.NoCancellationTemplateFileException;
import at.willinger.invoicer.infrastructure.storage.FileUtils;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class GetCancellationTemplateFileTest extends AbstractInvoiceTypeServiceTest {

  private InvoiceType invoiceType;

  private InvoiceTypeId invoiceTypeId;

  private InputStream templateContent;

  @BeforeEach
  public void setUp() {
    invoiceType = InvoiceTypeTestEntityFactory.getInvoiceTypeWithoutTemplates();
    invoiceTypeId = invoiceType.getId();

    invoiceTypeService.create(invoiceType);

    templateContent = InvoiceTypeTestEntityFactory.getTemplateFileInputStream();
  }

  @Test
  public void withNotExistingInvoiceTypeId_shouldThrowInvoiceTypeIdNotFoundExcpeption() {
    InvoiceTypeId notExistingId = new InvoiceTypeId("SOME_NOT_EXISTING_ID");

    InvoiceTypeIdNotFoundExcpeption foundEx = assertThrows(
        InvoiceTypeIdNotFoundExcpeption.class,
        () -> invoiceTypeService.getCancellationTemplateFile(notExistingId));

    assertEquals("Invalid Id should be mapped in exception",
        notExistingId,
        foundEx.getInvoiceTypeId());
  }

  @Test
  public void withNoTemplateUploaded_shouldThrowNoCancellationTemplateFileException() {
    NoCancellationTemplateFileException foundEx = assertThrows(
        NoCancellationTemplateFileException.class,
        () -> invoiceTypeService.getCancellationTemplateFile(invoiceTypeId));

    assertEquals("Id of the invoice type should be mapped",
        invoiceTypeId,
        foundEx.getInvoiceTypeId());
  }

  @Test
  public void shouldReturnTemplateAsInputStream() throws IOException {
    invoiceTypeService.uploadCancellationTemplateFile(
        invoiceTypeId, TEMPLATE_FILE_DIRECTORY, templateContent);

    InputStream resultIs = invoiceTypeService
        .getCancellationTemplateFile(invoiceTypeId);

    String resultString = IOUtils.toString(resultIs, StandardCharsets.UTF_16);
    String expectedString = IOUtils.toString(
        InvoiceTypeTestEntityFactory.getTemplateFileInputStream(),
        StandardCharsets.UTF_16);

    assertEquals("Content should be restored from storage",
        expectedString,
        resultString);

    FileUtils.closeInputStream(resultIs);
  }

}
