package at.willinger.invoicer.application.invoicetypeservice;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeStatus;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdNotFoundExcpeption;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class ActivateTest extends AbstractInvoiceTypeServiceTest {
	
	private static final String TEMPLATE_FILE_NAME = "someTemplate.jxml";
	
	private InvoiceType invoiceType;
	private InvoiceTypeId invoiceTypeId;
	
	@BeforeEach
	public void setUp() {
		invoiceType = InvoiceTypeTestEntityFactory.getInvoiceTypeWithoutTemplates();
		invoiceTypeId = invoiceType.getId();
		invoiceTypeService.create(invoiceType);
		
		invoiceTypeService.uploadTemplateFile(
				invoiceTypeId, 
				TEMPLATE_FILE_NAME,
				InvoiceTypeTestEntityFactory.getTemplateFileInputStream());
		invoiceTypeService.uploadCancellationTemplateFile(
				invoiceTypeId, 
				TEMPLATE_FILE_NAME,
				InvoiceTypeTestEntityFactory.getTemplateFileInputStream());
	}

	@Test
	public void withNotExistingInvoiceTypeId_shouldThrowInvoiceTypeIdNotFoundException() {
		InvoiceTypeId notExistingId = new InvoiceTypeId("SOME_NOT_EXISTING_ID");

		InvoiceTypeIdNotFoundExcpeption foundEx = assertThrows(
				InvoiceTypeIdNotFoundExcpeption.class,
				() -> invoiceTypeService.activate(notExistingId));

		assertEquals("Invalid Id should be mapped in exception",
				notExistingId,
				foundEx.getInvoiceTypeId());
	}

	@Test
	public void shouldActivateInvoiceType() {
		invoiceTypeService.activate(invoiceTypeId);

		assertEquals("InvoiceType should be active after operation",
				InvoiceTypeStatus.ACTIVE,
				invoiceType.getStatus());
	}

}
