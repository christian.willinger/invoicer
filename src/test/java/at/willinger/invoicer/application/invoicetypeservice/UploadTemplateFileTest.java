package at.willinger.invoicer.application.invoicetypeservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdNotFoundExcpeption;
import at.willinger.invoicer.infrastructure.storage.FileUtils;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class UploadTemplateFileTest extends AbstractInvoiceTypeServiceTest {

  private static final String TEMPLATE_FILE_NAME = "someTemplate.jxml";

  private InvoiceType invoiceType;
  private InvoiceTypeId invoiceTypeId;
  private InputStream templateContent;

  @BeforeEach
  public void setUp() {
    invoiceType = InvoiceTypeTestEntityFactory.getInvoiceTypeWithoutTemplates();
    invoiceTypeId = invoiceType.getId();
    invoiceTypeService.create(invoiceType);

    templateContent = InvoiceTypeTestEntityFactory.getTemplateFileInputStream();
  }

  @Test
  public void withNotExistingInvoiceTypeId_shouldThrowInvoiceTypeIdNotFoundExcpeption() {
    InvoiceTypeId notExistingId = new InvoiceTypeId("SOME_NOT_EXISTING_ID");

    InvoiceTypeIdNotFoundExcpeption foundEx = assertThrows(
        InvoiceTypeIdNotFoundExcpeption.class,
        () -> invoiceTypeService
            .uploadTemplateFile(notExistingId, TEMPLATE_FILE_NAME, templateContent));

    assertEquals("Invalid Id should be mapped in exception",
        notExistingId,
        foundEx.getInvoiceTypeId());
  }

  @Test
  public void shouldUpdateInvoiceType() {
    invoiceTypeService.uploadTemplateFile(
        invoiceTypeId, TEMPLATE_FILE_NAME, templateContent);

    assertEquals("Template file name should be set in template",
        TEMPLATE_FILE_NAME,
        invoiceType.getTemplateFileName());
  }

  @Test
  public void shouldStoreContent() throws FileNotFoundException, IOException {
    invoiceTypeService.uploadTemplateFile(
        invoiceTypeId, TEMPLATE_FILE_NAME, templateContent);

    InputStream resultStream = fileStorageService.loadInvoiceTemplate(invoiceType);

    assertTrue(IOUtils.contentEquals(
        InvoiceTypeTestEntityFactory.getTemplateFileInputStream(),
        resultStream));

    FileUtils.closeInputStream(resultStream);
  }

}
