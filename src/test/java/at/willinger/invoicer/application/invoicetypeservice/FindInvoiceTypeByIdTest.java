package at.willinger.invoicer.application.invoicetypeservice;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdNotFoundExcpeption;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTypeTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class FindInvoiceTypeByIdTest extends AbstractInvoiceTypeServiceTest {

  private InvoiceType invoiceType;
  private InvoiceTypeId invoiceTypeId;

  @BeforeEach
  public void setUp() {
    invoiceType = InvoiceTypeTestEntityFactory.getActiveInvoiceType1();
    invoiceTypeId = invoiceType.getId();

    invoiceTypeService.create(invoiceType);
  }

  @Test
  public void withNotExistingInvoiceTypeId_shouldThrowInvoiceTypeIdNotFoundExcpeption() {
    InvoiceTypeId notExistingId = new InvoiceTypeId("SOME_NOT_EXISTING_ID");

    InvoiceTypeIdNotFoundExcpeption foundEx = assertThrows(
        InvoiceTypeIdNotFoundExcpeption.class,
        () -> invoiceTypeService.findInvoiceTypeById(notExistingId));

    assertEquals("Invalid Id should be mapped in exception",
        notExistingId,
        foundEx.getInvoiceTypeId());
  }

  @Test
  public void shouldReturnInvoiceType() {
    InvoiceType result = invoiceTypeService.findInvoiceTypeById(invoiceTypeId);

    assertEquals(invoiceType, result);
  }

}
