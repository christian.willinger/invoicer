package at.willinger.invoicer.application.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdNotFoundException;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceNotRenderedException;
import at.willinger.invoicer.infrastructure.storage.FileUtils;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class GetInvoiceFileTest extends AbstractInvoiceServiceTest {
	
	private Invoice invoice;
	private InvoiceId invoiceId;
	
	@BeforeEach
	public void setUp() throws FileNotFoundException {
		invoice = InvoiceTestEntityFactory.getNewStandardInvoice1();
		invoiceId = invoice.getId();

		invoiceTypeRepository.persist(invoice.getInvoiceType());

		invoiceService.create(invoice);
		invoiceService.finalizeById(invoiceId);
		invoiceService.renderById(invoiceId);

		InputStream content = new FileInputStream("src/test/resources/template/INVOICE_1");
		fileStorageService.storeInvoiceFile(invoice, content);
	}

	@Test
	public void withNotExistingInvoiceId_shouldThrowInvoiceIdNotFoundException() {
		InvoiceId notExistingId = new InvoiceId("SOME_NOT_EXISTING_ID");

		InvoiceIdNotFoundException foundEx = assertThrows(
				InvoiceIdNotFoundException.class,
				() -> invoiceService.getInvoiceFile(notExistingId));

		assertEquals("Invalid Id should be mapped in exception",
				notExistingId,
				foundEx.getInvoiceId());
	}
	
	@Test
	public void withInvoiceNotRendered_shouldThrowInvoiceNotRenderedException() {
		Invoice newInvoice = InvoiceTestEntityFactory.getNewStandardInvoice2();
		InvoiceId newInvoiceId = newInvoice.getId();
		invoiceRepository.persist(newInvoice);

		InvoiceNotRenderedException foundEx = assertThrows(
				InvoiceNotRenderedException.class,
				() -> invoiceService.getInvoiceFile(newInvoiceId));

		assertEquals("Invoice ID should be mapped in exception",
				newInvoiceId,
				foundEx.getInvoiceId());
	}
	
	@Test
	public void shouldResolveCorrectFile() throws IOException {
		Charset charset = Charset.forName("UTF-8");
		
		InputStream expectedIs = new FileInputStream("src/test/resources/template/INVOICE_1");
		String expected = IOUtils.toString(expectedIs, charset);
		FileUtils.closeInputStream(expectedIs);
		
		InputStream resultIs = invoiceService.getInvoiceFile(invoiceId);
		String result = IOUtils.toString(resultIs, charset);
		FileUtils.closeInputStream(resultIs);
		
		assertEquals("Content of file identified by path in config file + invoice file name should be returned",
				expected,
				result);
	}
	
}
