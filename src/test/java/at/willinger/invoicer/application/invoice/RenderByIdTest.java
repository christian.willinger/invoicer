package at.willinger.invoicer.application.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdNotFoundException;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class RenderByIdTest extends AbstractInvoiceServiceTest {

	private Invoice invoice;
	private InvoiceId invoiceId;
	
	@BeforeEach
	public void setUp() {
		invoice = InvoiceTestEntityFactory.getNewStandardInvoice1();
		invoiceId = invoice.getId();

		InvoiceType invoiceType = invoice.getInvoiceType();
		invoiceTypeService.create(invoiceType);

		invoiceService.create(invoice);
		invoiceService.finalizeById(invoiceId);
	}
	
	@Test
	public void withNotExistingInvoiceId_shouldThrowInvoiceIdNotFoundException() {
		InvoiceId notExistingId = new InvoiceId("SOME_NOT_EXISTING_ID");

		InvoiceIdNotFoundException foundEx = assertThrows(
				InvoiceIdNotFoundException.class,
				() -> invoiceService.renderById(notExistingId));

		assertEquals("Invalid Id should be mapped in exception",
				notExistingId,
				foundEx.getInvoiceId());
	}

	@Test
	public void shouldUpdateInvoiceStatus() {
		invoiceService.renderById(invoiceId);

		assertTrue(invoice.getRendered());
	}
	
	@Test
	public void shouldRenderDocument() {
		invoiceService.renderById(invoiceId);
		
		Mockito.verify(documentRenderer, Mockito.times(1))
			.render(invoice);
	}
	
}
