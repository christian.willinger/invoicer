package at.willinger.invoicer.application.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.InvoiceStatus;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdNotFoundException;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class FinalizeByIdTest extends AbstractInvoiceServiceTest {

	private Invoice invoice;
	private InvoiceId invoiceId;
	private InvoiceType invoiceType;
	private InvoiceTypeId invoiceTypeId;
	
	@BeforeEach
	public void setUp() {
		invoice = InvoiceTestEntityFactory.getNewStandardInvoice1();
		invoiceId = invoice.getId();
		invoiceType = invoice.getInvoiceType();
		invoiceTypeId = invoiceType.getId();

		invoiceTypeService.create(invoiceType);

		invoiceService.create(invoice);
	}
	
	@Test
	public void withNotExistingInvoiceId_shouldThrowInvoiceIdNotFoundException() {
		InvoiceId notExistingId = new InvoiceId("SOME_NOT_EXISTING_ID");

		InvoiceIdNotFoundException foundEx = assertThrows(
				InvoiceIdNotFoundException.class,
				() -> invoiceService.finalizeById(notExistingId));

		assertEquals("Invalid Id should be mapped in exception",
				notExistingId,
				foundEx.getInvoiceId());
	}
	
	@Test
	public void withInvoiceTypeIdForInvoiceNotExists_shouldThrowIllegalStateException() {
		resetInvoiceTypeRepository();

		IllegalStateException foundEx = assertThrows(
				IllegalStateException.class,
				() -> invoiceService.finalizeById(invoiceId));

		assertTrue("Error message should contain missing invoice id",
				foundEx.getMessage().contains(invoiceId.getValue()));
	}
	
	@Test
	public void shouldIncrementInvoiceTypeSerial() {
		long serialBeforeUpdate = invoiceType.getSerialSequence().getValue();
		
		invoiceService.finalizeById(invoiceId);

		long expectedSerial = serialBeforeUpdate + 1;
		assertEquals("Serial should be old serial + 1",
				expectedSerial, 
				invoiceType.getSerialSequence().getValue().longValue());
	}

	@Test
	public void shouldUpdateInvoiceStatus() {
		invoiceService.finalizeById(invoiceId);

		assertEquals(
				InvoiceStatus.FINALIZED,
				invoice.getStatus());
	}
	
}
