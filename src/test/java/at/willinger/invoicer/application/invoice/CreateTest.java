package at.willinger.invoicer.application.invoice;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdConflictException;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class CreateTest extends AbstractInvoiceServiceTest {

	private Invoice invoice;
	private InvoiceId invoiceId;
	
	@BeforeEach
	public void setUp() {
		invoice = InvoiceTestEntityFactory.getNewStandardInvoice1();
		invoiceId = invoice.getId();
	}
	
	@Test
	public void withInvoiceIdAlreadyUsed_shouldThrowInvoiceIdConflictException() {
		invoiceService.create(invoice);

		InvoiceIdConflictException foundEx = assertThrows(
				InvoiceIdConflictException.class,
				() -> invoiceService.create(invoice));

		assertEquals("Conflicting invoice id should be set in exception",
				invoice.getId(),
				foundEx.getInvoiceId());
	}

	@Test
	public void shouldPersistInvoice() {
		invoiceService.create(invoice);

		assertTrue("Invoice should be persisted after create",
			invoiceRepository.findById(invoiceId).isPresent());
	}
	
}
