package at.willinger.invoicer.application.invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdNotFoundException;
import at.willinger.invoicer.utils.categories.TestCategoryTag;
import at.willinger.invoicer.utils.factory.entity.InvoiceTestEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@Tag(TestCategoryTag.SERVICE_UNIT)
public class PassInvoiceToAccountingTest extends AbstractInvoiceServiceTest {

	private Invoice invoice;
	private InvoiceId invoiceId;
	
	@BeforeEach
	public void setUp() {
		invoice = InvoiceTestEntityFactory.getNewStandardInvoice1();
		invoiceId = invoice.getId();


		invoiceTypeRepository.persist(invoice.getInvoiceType());

		invoiceService.create(invoice);
		invoiceService.finalizeById(invoiceId);
	}
	
	@Test
	public void withNotExistingInvoiceId_shouldThrowInvoiceIdNotFoundException() {
		InvoiceId notExistingId = new InvoiceId("SOME_NOT_EXISTING_ID");

		InvoiceIdNotFoundException foundEx = assertThrows(
				InvoiceIdNotFoundException.class,
				() -> invoiceService.passInvoiceToAccounting(notExistingId));

		assertEquals("Invalid Id should be mapped in exception",
				notExistingId,
				foundEx.getInvoiceId());
	}
	
	@Test
	public void shouldInvokeAccountingService() {
		invoiceService.passInvoiceToAccounting(invoiceId);
		
		Mockito.verify(accountingMockService, Mockito.times(1))
			.passInvoiceToAccounting(invoice);
	}

}
