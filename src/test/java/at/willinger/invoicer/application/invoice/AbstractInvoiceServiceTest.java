package at.willinger.invoicer.application.invoice;

import at.willinger.invoicer.application.InvoiceService;
import at.willinger.invoicer.application.InvoiceTypeService;
import at.willinger.invoicer.application.impl.InvoiceServiceImpl;
import at.willinger.invoicer.application.impl.InvoiceTypeServiceImpl;
import at.willinger.invoicer.domain.model.invoice.InvoiceRepository;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeRepository;
import at.willinger.invoicer.domain.service.AccountingService;
import at.willinger.invoicer.domain.service.FileStorageService;
import at.willinger.invoicer.infrastructure.rendering.DocumentRenderer;
import at.willinger.invoicer.infrastructure.spring.applicationconfig.InvoiceConfig;
import at.willinger.invoicer.infrastructure.spring.applicationconfig.InvoiceTypeConfig;
import at.willinger.invoicer.infrastructure.storage.LocalFilesystemFileStorageService;
import at.willinger.invoicer.utils.repository.InvoiceInMemoryRepository;
import at.willinger.invoicer.utils.repository.InvoiceTypeInMemoryRepository;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

public abstract class AbstractInvoiceServiceTest {
	
	protected static final String TEMPLATE_FILE_DIRECTORY = "target/teststorage";
	
	protected InvoiceTypeRepository invoiceTypeRepository;
	protected InvoiceRepository invoiceRepository;
	protected InvoiceTypeService invoiceTypeService;
	protected InvoiceService invoiceService;
	protected FileStorageService fileStorageService;
	protected AccountingService accountingMockService;
	protected DocumentRenderer documentRenderer;

	private InvoiceTypeInMemoryRepository invoiceTypeInMemoryRepository;
	
	@BeforeEach
	public void setUpInfrastructure() {
		invoiceTypeInMemoryRepository = new InvoiceTypeInMemoryRepository();
		invoiceTypeRepository = invoiceTypeInMemoryRepository;

		invoiceRepository = new InvoiceInMemoryRepository();
		
		InvoiceTypeConfig invoiceTypeConfig = new InvoiceTypeConfig();
		invoiceTypeConfig.setTemplateFileDirectory(TEMPLATE_FILE_DIRECTORY);
		
		InvoiceConfig invoiceConfig = new InvoiceConfig();
		invoiceConfig.setDocumentFileDirectory(TEMPLATE_FILE_DIRECTORY);
		
		accountingMockService = Mockito.mock(AccountingService.class);
		
		LocalFilesystemFileStorageService localFilesystemFileStorageService = new LocalFilesystemFileStorageService();
		localFilesystemFileStorageService.setInvoiceTypeConfig(invoiceTypeConfig);
		localFilesystemFileStorageService.setInvoiceConfig(invoiceConfig);
		fileStorageService = localFilesystemFileStorageService;
		
		documentRenderer = Mockito.mock(DocumentRenderer.class);

		InvoiceTypeServiceImpl invoiceTypeServiceImpl = new InvoiceTypeServiceImpl();
		invoiceTypeServiceImpl.setInvoiceTypeRepository(invoiceTypeRepository);
		invoiceTypeServiceImpl.setFileStorageService(fileStorageService);
		invoiceTypeService = invoiceTypeServiceImpl;
		
		InvoiceServiceImpl invoiceServiceImpl = new InvoiceServiceImpl();
		invoiceServiceImpl.setFileStorageService(localFilesystemFileStorageService);
		invoiceServiceImpl.setInvoiceRepository(invoiceRepository);
		invoiceServiceImpl.setInvoiceTypeRepository(invoiceTypeRepository);
		invoiceServiceImpl.setAccountingService(accountingMockService);
		invoiceServiceImpl.setDocumentRenderer(documentRenderer);
		invoiceService = invoiceServiceImpl;
	}
	
	@After
	public void tearDownInfrastructure() throws IOException {
		File folder = FileSystems.getDefault()
				.getPath(TEMPLATE_FILE_DIRECTORY)
				.toFile();
		
		org.apache.commons.io.FileUtils.deleteDirectory(folder);
	}

	protected void resetInvoiceTypeRepository() {
		invoiceTypeInMemoryRepository.reset();
	}

}
