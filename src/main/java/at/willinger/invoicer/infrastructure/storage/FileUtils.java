package at.willinger.invoicer.infrastructure.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;

public class FileUtils {

	private FileUtils() {
	}

	public static FileInputStream getFileInputStream(File file) {
		try {
			return new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw new UncheckedIOException(e);
		}
	}
	
	public static FileOutputStream getFileOutputStream(File file) {
		try {
			return new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			throw new UncheckedIOException(e);
		}
	}
	
	public static void copyInputStreamToFile(final InputStream source, final File destination) {
		try {
			org.apache.commons.io.FileUtils.copyInputStreamToFile(source, destination);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public static void closeInputStream(InputStream is) {
		try {
			is.close();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
	
}
