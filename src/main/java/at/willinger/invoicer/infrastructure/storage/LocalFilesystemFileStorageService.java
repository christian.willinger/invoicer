package at.willinger.invoicer.infrastructure.storage;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.service.FileStorageService;
import at.willinger.invoicer.infrastructure.spring.applicationconfig.InvoiceConfig;
import at.willinger.invoicer.infrastructure.spring.applicationconfig.InvoiceTypeConfig;
import java.io.File;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocalFilesystemFileStorageService implements FileStorageService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String TEMPLATE_FILE_POSTFIX = "template";
	private static final String STORNO_TEMPLATE_FILE_POSTFIX = "storno_template";
	private static final String INVOICE_FILE_PREFIX = "invoice";

	@Autowired
	private InvoiceTypeConfig invoiceTypeConfig;

	@Autowired
	private InvoiceConfig invoiceConfig;

	@Override
	public void storeInvoiceTemplate(InvoiceType invoiceType, InputStream is) {
		logger.info("Save template file for {}",
				invoiceType);

		File file = getTemplateFile(invoiceType, TEMPLATE_FILE_POSTFIX);
		logger.info("File is written to: {}", file);

		FileUtils.copyInputStreamToFile(is, file);
		FileUtils.closeInputStream(is);
	}

	@Override
	public InputStream loadInvoiceTemplate(InvoiceType invoiceType) {
		logger.debug("Load template file for {}",
				invoiceType);

		File file = getTemplateFile(invoiceType, TEMPLATE_FILE_POSTFIX);
		logger.info("File is loaded from: {}", file);

		validateFileExists(file);

		return FileUtils.getFileInputStream(file);
	}

	@Override
	public void storeInvoiceStornoTemplate(InvoiceType invoiceType, InputStream is) {
		logger.info("Save storno template file for {}",
				invoiceType);

		File file = getTemplateFile(invoiceType, STORNO_TEMPLATE_FILE_POSTFIX);
		logger.info("File is written to: {}", file);

		FileUtils.copyInputStreamToFile(is, file);
		FileUtils.closeInputStream(is);
	}

	@Override
	public InputStream loadInvoiceStornoTemplate(InvoiceType invoiceType) {
		logger.debug("Load storno template file for {}",
				invoiceType);

		File file = getTemplateFile(invoiceType, STORNO_TEMPLATE_FILE_POSTFIX);
		logger.info("File is loaded from: {}", file);

		validateFileExists(file);

		return FileUtils.getFileInputStream(file);
	}

	private File getTemplateFile(InvoiceType invoiceType, String postfix) {
		String templateFile = String.format("%s_%s",
				invoiceType.getId().getValue(),
				postfix);

		return invoiceTypeConfig.getTemplateFileDirectoryPath()
				.resolve(templateFile)
				.toFile();
	}

	@Override
	public void storeInvoiceFile(Invoice invoice, InputStream is) {
		logger.info("Save invoice file for {}",
				invoice);

		File file = getInvoiceFile(invoice);
		logger.info("File is written to: {}", file);

		FileUtils.copyInputStreamToFile(is, file);
		FileUtils.closeInputStream(is);
	}

	@Override
	public InputStream loadInvoiceFile(Invoice invoice) throws IllegalStateException {
		logger.debug("Load invoice file for {}",
				invoice);

		File file = getInvoiceFile(invoice);
		logger.info("File is loaded from: {}", file);

		validateFileExists(file);

		return FileUtils.getFileInputStream(file);
	}

	private File getInvoiceFile(Invoice invoice) {
		String invoiceFile = String.format("%s_%d",
				INVOICE_FILE_PREFIX,
				invoice.getSerial().getValue());

		return invoiceConfig.getDocumentFileDirectoryPath()
				.resolve(invoiceFile)
				.toFile();
	}

	private void validateFileExists(File file) {
		if(!file.exists()) {
			throw new IllegalStateException("Cannot load file " + file);
		}
	}

	public void setInvoiceTypeConfig(InvoiceTypeConfig invoiceTypeConfig) {
		this.invoiceTypeConfig = invoiceTypeConfig;
	}

	public void setInvoiceConfig(InvoiceConfig invoiceConfig) {
		this.invoiceConfig = invoiceConfig;
	}

}
