package at.willinger.invoicer.infrastructure.spring.applicationconfig;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@ConfigurationProperties(prefix = "invoicer.invoice-type")
@Validated
public class InvoiceTypeConfig {
	
	@NotNull
	@Size(min = 1)
	private String templateFileDirectory;
	
	@PostConstruct
	public void init() {
		if(!getTemplateFileDirectoryPath().toFile().exists()) {
			String msg = String.format("Path for templateFileDirectory (%s) does not exists",
					templateFileDirectory);
			throw new IllegalStateException(msg);
		}
	}
	
	public Path getTemplateFileDirectoryPath() {
		return FileSystems.getDefault()
				.getPath(templateFileDirectory);
	}
	

	public String getTemplateFileDirectory() {
		return templateFileDirectory;
	}

	public void setTemplateFileDirectory(String templateFileDirectory) {
		this.templateFileDirectory = templateFileDirectory;
	}

}
