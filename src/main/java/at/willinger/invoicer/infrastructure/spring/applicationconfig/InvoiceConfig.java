package at.willinger.invoicer.infrastructure.spring.applicationconfig;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@ConfigurationProperties(prefix = "invoicer.invoice")
@Validated
public class InvoiceConfig {

	@NotNull
	@Size(min = 1)
	private String documentFileDirectory;
	
	@PostConstruct
	public void init() {
		if(!getDocumentFileDirectoryPath().toFile().exists()) {
			String msg = String.format("Path for documentFileDirectory (%s) does not exists",
					documentFileDirectory);
			throw new IllegalStateException(msg);
		}
	}

	public Path getDocumentFileDirectoryPath() {
		return FileSystems.getDefault()
				.getPath(documentFileDirectory);
	}

	public String getDocumentFileDirectory() {
		return documentFileDirectory;
	}

	public void setDocumentFileDirectory(String documentFileDirectory) {
		this.documentFileDirectory = documentFileDirectory;
	}

}
