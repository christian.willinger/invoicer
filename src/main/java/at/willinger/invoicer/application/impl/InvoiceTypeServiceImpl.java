package at.willinger.invoicer.application.impl;

import at.willinger.invoicer.application.InvoiceTypeService;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeRepository;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvalidInvoiceTypeStatusException;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdConflictException;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdNotFoundExcpeption;
import at.willinger.invoicer.domain.model.invoicetype.exception.NoCancellationTemplateFileException;
import at.willinger.invoicer.domain.model.invoicetype.exception.NoTemplateFileException;
import at.willinger.invoicer.domain.service.FileStorageService;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceTypeServiceImpl implements InvoiceTypeService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private InvoiceTypeRepository invoiceTypeRepository;
	
	@Autowired
	private FileStorageService fileStorageService;

	@Override
	public InvoiceType findInvoiceTypeById(InvoiceTypeId id)
			throws InvoiceTypeIdNotFoundExcpeption {
		return invoiceTypeRepository.findById(id)
				.orElseThrow(() -> new InvoiceTypeIdNotFoundExcpeption(id));
	}
	
	@Override
	public void create(InvoiceType invoiceType) 
			throws InvoiceTypeIdConflictException {
		checkInvoiceTypeIdConflict(invoiceType.getId());
		
		invoiceTypeRepository.persist(invoiceType);
		logger.info("{} created", invoiceType);
	}
	
	private void checkInvoiceTypeIdConflict(InvoiceTypeId invoiceTypeId) 
			throws InvoiceTypeIdConflictException {
		if(invoiceTypeRepository.findById(invoiceTypeId).isPresent()) {
			throw new InvoiceTypeIdConflictException(invoiceTypeId);
		}
	}
	
	@Override
	public void uploadTemplateFile(InvoiceTypeId id, String filename, InputStream is)
			throws InvoiceTypeIdNotFoundExcpeption, InvalidInvoiceTypeStatusException {
		InvoiceType invoiceType = invoiceTypeRepository
				.findAndLockById(id)
				.orElseThrow(() -> new InvoiceTypeIdNotFoundExcpeption(id));
		
		invoiceType.setTemplateFileName(filename);

		fileStorageService.storeInvoiceTemplate(invoiceType, is);
		
		invoiceTypeRepository.persist(invoiceType);
		logger.info("Render template [{}] uploaed for {}", 
				filename, invoiceType);
	}
	
	@Override
	public void uploadCancellationTemplateFile(InvoiceTypeId id, String filename, InputStream is)
			throws InvoiceTypeIdNotFoundExcpeption, InvalidInvoiceTypeStatusException {
		InvoiceType invoiceType = invoiceTypeRepository
				.findAndLockById(id)
				.orElseThrow(() -> new InvoiceTypeIdNotFoundExcpeption(id));
		
		invoiceType.setCancellationTemplateFileName(filename);
		
		fileStorageService.storeInvoiceStornoTemplate(invoiceType, is);
		
		invoiceTypeRepository.persist(invoiceType);
		logger.info("Storno render template [{}] uploaded for {}",
				filename, invoiceType);
	}
	
	@Override
	public void activate(InvoiceTypeId id)
			throws InvoiceTypeIdNotFoundExcpeption, InvalidInvoiceTypeStatusException,
			NoTemplateFileException {
		InvoiceType invoiceType = invoiceTypeRepository
				.findAndLockById(id)
				.orElseThrow(() -> new InvoiceTypeIdNotFoundExcpeption(id));

		invoiceType.activate();
		
		invoiceTypeRepository.persist(invoiceType);
		logger.info("{} activated",
				invoiceType);
	}
	
	@Override
	public InputStream getTemplateFile(InvoiceTypeId id)
			throws InvoiceTypeIdNotFoundExcpeption, NoTemplateFileException {
		InvoiceType invoiceType = invoiceTypeRepository
				.findById(id)
				.orElseThrow(() -> new InvoiceTypeIdNotFoundExcpeption(id));

		if(!invoiceType.hasTemplateFile()) {
			throw new NoTemplateFileException(invoiceType);
		}
		
		return fileStorageService.loadInvoiceTemplate(invoiceType);
	}
	
	@Override
	public InputStream getCancellationTemplateFile(InvoiceTypeId id)
			throws InvoiceTypeIdNotFoundExcpeption, NoCancellationTemplateFileException {
		InvoiceType invoiceType = invoiceTypeRepository
				.findById(id)
				.orElseThrow(() -> new InvoiceTypeIdNotFoundExcpeption(id));

		if(!invoiceType.hasCancellationTemplateFile()) {
			throw new NoCancellationTemplateFileException(invoiceType);
		}
		
		return fileStorageService.loadInvoiceStornoTemplate(invoiceType);
	}
	
	public void setInvoiceTypeRepository(InvoiceTypeRepository invoiceTypeRepository) {
		this.invoiceTypeRepository = invoiceTypeRepository;
	}

	public void setFileStorageService(FileStorageService fileStorageService) {
		this.fileStorageService = fileStorageService;
	}

}
