package at.willinger.invoicer.application.impl;

import at.willinger.invoicer.application.InvoiceService;
import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.InvoiceRepository;
import at.willinger.invoicer.domain.model.invoice.Serial;
import at.willinger.invoicer.domain.model.invoice.exception.InvalidInvoiceStatusException;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdConflictException;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdNotFoundException;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceNotRenderedException;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeRepository;
import at.willinger.invoicer.domain.service.AccountingService;
import at.willinger.invoicer.domain.service.FileStorageService;
import at.willinger.invoicer.infrastructure.rendering.DocumentRenderer;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private InvoiceRepository invoiceRepository;
	
	@Autowired
	private InvoiceTypeRepository invoiceTypeRepository;
	
	@Autowired
	private DocumentRenderer documentRenderer;
	
	@Autowired
	private AccountingService accountingService;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Override
	public void create(Invoice invoice) {
		checkInvoiceIdConflict(invoice.getId());
		invoice = invoiceRepository.persist(invoice);
		logger.info("{} saved", invoice);
	}
	
	private void checkInvoiceIdConflict(InvoiceId invoiceId) {
		if(invoiceRepository.findById(invoiceId).isPresent()) {
			throw new InvoiceIdConflictException(invoiceId);
		}
	}

	@Override
	public void finalizeById(InvoiceId id)
			throws InvalidInvoiceStatusException {
		Invoice invoice = invoiceRepository
				.findAndLockById(id)
				.orElseThrow(() -> new InvoiceIdNotFoundException(id));
	
		InvoiceTypeId invoiceTypeId = invoice.getInvoiceType().getId();
		InvoiceType invoiceType = invoiceTypeRepository
				.findAndLockById(invoiceTypeId)
				.orElseThrow(() -> new IllegalStateException("Cannot load Type of Invoice " + id));
		
		Serial serial = invoiceType.getSerialSequence().getSerial();
		invoice.finalizeWithSerial(serial);
		
		invoiceType.incrementSerial();
		logger.debug("Serial of {} incremented to {}", 
				invoiceType,
				invoiceType.getSerialSequence());
		
		invoice = invoiceRepository.persist(invoice);
		logger.info("{} finalized with serial [{}]", invoice, serial);
	}

	@Override
	public void renderById(InvoiceId id) throws InvalidInvoiceStatusException {
		Invoice invoice = invoiceRepository
				.findAndLockById(id)
				.orElseThrow(() -> new InvoiceIdNotFoundException(id));
		
		invoice.render();
		documentRenderer.render(invoice);
		
		invoice = invoiceRepository.persist(invoice);
		logger.info("{} document rendered", invoice);
	}
	
	@Override
	public void passInvoiceToAccounting(InvoiceId id) throws InvoiceIdNotFoundException {
		Invoice invoice = invoiceRepository
				.findAndLockById(id)
				.orElseThrow(() -> new InvoiceIdNotFoundException(id));
		
		accountingService.passInvoiceToAccounting(invoice);
		logger.info("{} accounted", invoice);
	}
	
	@Override
	public InputStream getInvoiceFile(InvoiceId invoiceId)
			throws InvoiceIdNotFoundException, InvoiceNotRenderedException {
		Invoice invoice = invoiceRepository
				.findById(invoiceId)
				.orElseThrow(() -> new InvoiceIdNotFoundException(invoiceId));
		
		if(!invoice.getRendered()) {
			throw new InvoiceNotRenderedException(invoiceId);
		}
		
		return fileStorageService.loadInvoiceFile(invoice);
	}

	public void setInvoiceRepository(InvoiceRepository invoiceRepository) {
		this.invoiceRepository = invoiceRepository;
	}

	public void setInvoiceTypeRepository(InvoiceTypeRepository invoiceTypeRepository) {
		this.invoiceTypeRepository = invoiceTypeRepository;
	}

	public void setDocumentRenderer(DocumentRenderer documentRenderer) {
		this.documentRenderer = documentRenderer;
	}

	public void setAccountingService(AccountingService accountingService) {
		this.accountingService = accountingService;
	}

	public void setFileStorageService(FileStorageService fileStorageService) {
		this.fileStorageService = fileStorageService;
	}

}
