package at.willinger.invoicer.application;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeStatus;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvalidInvoiceTypeStatusException;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdConflictException;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdNotFoundExcpeption;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeNotValidException;
import at.willinger.invoicer.domain.model.invoicetype.exception.NoCancellationTemplateFileException;
import at.willinger.invoicer.domain.model.invoicetype.exception.NoTemplateFileException;
import java.io.InputStream;

public interface InvoiceTypeService {
	
	/**
	 * Finds an {@link InvoiceType} by its ID.
	 * 
	 * @param id ID of the {@link InvoiceType} to lookup.
	 * @return Found Entity.
	 * @throws InvoiceTypeIdNotFoundExcpeption
	 * 			If no {@link InvoiceType} was found for the given ID.
	 */
	InvoiceType findInvoiceTypeById(InvoiceTypeId id)
		throws InvoiceTypeIdNotFoundExcpeption;
	
	/**
	 * Creates a new invoice type.
	 * 
	 * @param invoiceType Entity to create
	 * @throws InvoiceTypeIdConflictException
	 * 			If already another {@link InvoiceType} with the same ID exists.
	 */
	void create(InvoiceType invoiceType)
		throws InvoiceTypeIdConflictException;
	
	/**
	 * Updates the template file for rendering a standard invoice of an invoice type.
	 *  
	 * @param id ID of the {@link InvoiceType}.
	 * @param filename Name of the file when it was uploaded.
	 * @param is Template file as {@link InputStream}.
	 * @throws InvoiceTypeIdNotFoundExcpeption
	 * 			If no {@link InvoiceType} was found for the given ID.
	 */
	void uploadTemplateFile(InvoiceTypeId id, String filename, InputStream is)
			throws InvoiceTypeIdNotFoundExcpeption;
	
	/**
	 * Updates the template file for rendering a cancellation invoice of an invoice type.
	 *  
	 * @param id ID of the {@link InvoiceType}.
	 * @param filename Name of the file when it was uploaded.
	 * @param is Template file as {@link InputStream}.
	 * @throws InvoiceTypeIdNotFoundExcpeption
	 * 			If no {@link InvoiceType} was found for the given ID.
	 */
	void uploadCancellationTemplateFile(InvoiceTypeId id, String filename, InputStream is)
			throws InvoiceTypeIdNotFoundExcpeption;

	/**
	 * Activates an invoice type to be used for invoicing.
	 * 
	 * @param id ID of the {@link InvoiceType}.
	 * @throws InvoiceTypeIdNotFoundExcpeption
	 * 			If no {@link InvoiceType} was found for the given ID.
	 * @throws InvalidInvoiceTypeStatusException
	 * 			If the invoice type is not in {@link InvoiceTypeStatus#NEW}.
	 * @throws InvoiceTypeNotValidException
	 * 			If the invoice type is not configured in a state to be activated.
	 * 			E.g.: Missing template.
	 */
	void activate(InvoiceTypeId id)
		throws InvoiceTypeIdNotFoundExcpeption, InvalidInvoiceTypeStatusException,
            InvoiceTypeNotValidException;
	
	/**
	 * Returns the template file for rendering a standard invoice of an invoice type.
	 *
	 * @param id ID of the {@link InvoiceType}.
	 * @return Template file as {@link InputStream}.
	 * @throws InvoiceTypeIdNotFoundExcpeption
	 * 			If no {@link InvoiceType} was found for the given ID.
	 * @throws NoTemplateFileException
	 * 			If the template has no render file uploaed.
	 */
	InputStream getTemplateFile(InvoiceTypeId id)
			throws InvoiceTypeIdNotFoundExcpeption, NoTemplateFileException;
	
	/**
	 * Returns the template file for rendering a cancellation invoice of an invoice type.
	 *
	 * @param id ID of the {@link InvoiceType}.
	 * @return Storno template file as {@link InputStream}.
	 * @throws InvoiceTypeIdNotFoundExcpeption
	 * 			If no {@link InvoiceType} was found for the given ID.
	 * @throws NoCancellationTemplateFileException
	 * 			If the template has no storno render file uploaed.
	 */
	InputStream getCancellationTemplateFile(InvoiceTypeId id)
			throws InvoiceTypeIdNotFoundExcpeption, NoCancellationTemplateFileException;
	
}
