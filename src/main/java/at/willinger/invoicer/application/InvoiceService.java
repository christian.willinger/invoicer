package at.willinger.invoicer.application;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.InvoiceStatus;
import at.willinger.invoicer.domain.model.invoice.exception.InvalidInvoiceStatusException;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdConflictException;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceIdNotFoundException;
import at.willinger.invoicer.domain.model.invoice.exception.InvoiceNotRenderedException;
import java.io.InputStream;

public interface InvoiceService {
	
	/**
	 * Persists a new invoice.
	 * 
	 * @param invoice Entity to persist.
	 * @throws InvoiceIdConflictException
	 * 			If the ID is already used by another invoice.
	 */
	void create(Invoice invoice)
			throws InvoiceIdConflictException;

	/**
	 * Finalizes a new invoice.
	 * This locks an invoice for update and generates it serial.
	 * 
	 * @param id Id of the {@link Invoice} to process.
	 * @throws InvoiceIdNotFoundException
	 * 			If no {@link Invoice} found for the given id.
	 * @throws InvalidInvoiceStatusException
	 * 			If the {@link Invoice} is not in {@link InvoiceStatus#NEW}.
	 */
	void finalizeById(InvoiceId id)
			throws InvoiceIdNotFoundException, InvalidInvoiceStatusException;
	
	/**
	 * Creates the document for a finalized invoice.
	 * 
	 * @param id Id of the {@link Invoice} to process.
	 * @throws InvoiceIdNotFoundException
	 * 			If no {@link Invoice} found for the given id.
	 * @throws InvalidInvoiceStatusException
	 * 			If the {@link Invoice} is in {@link InvoiceStatus#NEW}.
	 */
	void renderById(InvoiceId id)
			throws InvoiceIdNotFoundException, InvalidInvoiceStatusException;
	
	/**
	 * Passes an invoice to the accounting module to create an account line.
	 * 
	 * @param id Id of the {@link Invoice} to process.
	 * @throws InvoiceIdNotFoundException
	 * 			If no {@link Invoice} found for the given id.
	 * @throws InvalidInvoiceStatusException
	 * 			If the {@link Invoice} is in {@link InvoiceStatus#NEW}.
	 */
	void passInvoiceToAccounting(InvoiceId id)
			throws InvoiceIdNotFoundException, InvalidInvoiceStatusException;

	/**
	 * Returns the invoice as rendered PDF.
	 * 
	 * @param id Id of the {@link Invoice}.
	 * @return Content as PDF.
	 * @throws InvoiceIdNotFoundException
	 * 			If no {@link Invoice} found for the given id.
	 * @throws InvoiceNotRenderedException
	 * 			If the {@link Invoice} is not rendered yet.
	 */
	InputStream getInvoiceFile(InvoiceId id)
			throws InvoiceIdNotFoundException, InvoiceNotRenderedException;

}
