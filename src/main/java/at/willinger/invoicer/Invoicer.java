package at.willinger.invoicer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Invoicer {

  public static void main(String[] args) {
    SpringApplication.run(Invoicer.class, args);
  }

}
