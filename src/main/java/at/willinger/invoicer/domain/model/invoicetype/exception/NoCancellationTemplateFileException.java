package at.willinger.invoicer.domain.model.invoicetype.exception;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.shared.BaseException;

/**
 * If the Template is used for an operation which requires a storno render file but has none.
 * 
 * @author Christian Willinger
 *
 */
@SuppressWarnings("serial")
public class NoCancellationTemplateFileException extends BaseException {
	
	private final InvoiceTypeId invoiceTypeId;
	
	public NoCancellationTemplateFileException(InvoiceType invoiceType) {
		super(renderMessage(invoiceType));
		
		this.invoiceTypeId = invoiceType.getId();
	}
	
	private static String renderMessage(InvoiceType invoiceType) {
		return String.format("[%s] has no storno rendering file",
				invoiceType);
	}

	public InvoiceTypeId getInvoiceTypeId() {
		return invoiceTypeId;
	}

	@Override
	public String getErrorCode() {
		return "NO_TEMPLATE_STORNO";
	}
	
}
