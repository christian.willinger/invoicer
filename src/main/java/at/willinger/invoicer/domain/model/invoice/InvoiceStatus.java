package at.willinger.invoicer.domain.model.invoice;

/**
 * Represents the lifecycle stautses of an Invoice
 * 
 * @author Christian Willinger
 *
 */
public enum InvoiceStatus {

	/**
	 * A new invoice is considered as a draft.
	 * It has no serial and does not contribute to accounting.
	 * In this status an invoice is allowed to be deleted or modified.
	 */
	NEW,

	/**
	 * Invoice has a serial and must not be modified in terms of financial values.
	 */
	FINALIZED,
	
	/**
	 * Invoice is canceled by a cancellation invoice.
	 */
	CANCELED,
	
}
