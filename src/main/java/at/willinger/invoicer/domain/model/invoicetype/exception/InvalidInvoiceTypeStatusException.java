package at.willinger.invoicer.domain.model.invoicetype.exception;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeStatus;
import at.willinger.invoicer.domain.model.shared.BaseException;
import java.util.Set;
import org.springframework.util.StringUtils;

/**
 * Is thrown if an {@link InvoiceType} is in the wrong {@link InvoiceTypeStatus} for a given operation.
 * 
 * @author Christian Willinger
 *
 */
@SuppressWarnings("serial")
public class InvalidInvoiceTypeStatusException extends BaseException {
	
	private final InvoiceTypeId invoiceTypeId;
	
	private final InvoiceTypeStatus actualStatus;
	
	private final Set<InvoiceTypeStatus> expectedStatuses;

	public InvalidInvoiceTypeStatusException(InvoiceType invoiceType, Set<InvoiceTypeStatus> expectedStatuses) {
		super(renderMessage(invoiceType, expectedStatuses));
		this.invoiceTypeId = invoiceType.getId();
		this.actualStatus = invoiceType.getStatus();
		this.expectedStatuses = expectedStatuses;
	}
	
	private static String renderMessage(InvoiceType invoiceType, Set<InvoiceTypeStatus> expectedStatuses) {
		return String.format("[%s] is in status [%s] but allowed is [%s]",
				invoiceType,
				invoiceType.getStatus(),
				StringUtils.collectionToCommaDelimitedString(expectedStatuses));
	}

	public InvoiceTypeId getInvoiceTypeId() {
		return invoiceTypeId;
	}

	public InvoiceTypeStatus getActualStatus() {
		return actualStatus;
	}

	public Set<InvoiceTypeStatus> getExpectedStatuses() {
		return expectedStatuses;
	}

	@Override
	public String getErrorCode() {
		return "INVALID_INVOICE_TYPE_STATUS";
	}
	
}
