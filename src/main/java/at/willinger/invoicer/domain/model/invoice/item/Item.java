package at.willinger.invoicer.domain.model.invoice.item;

import static at.willinger.invoicer.domain.model.shared.RoundingContext.PRECISSION_DIGITS;
import static at.willinger.invoicer.domain.model.shared.RoundingContext.ROUNDING_MODE;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.Assert;

/**
 * Single line entry in the invoice.
 *
 * @author Christian Willinger
 */
@Entity
public class Item {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String description;

  @Enumerated(EnumType.STRING)
  private Unit chargedUnit;

  private BigInteger chargedAmount;

  private Tax tax;

  private BigDecimal taxPrice;

  private BigDecimal grossPrice;

  private BigDecimal netPrice;

  public Item(String description, Unit chargedUnit, BigInteger chargedAmount, Tax tax,
      BigDecimal netPrice) {
    Assert.hasText(description, "Description must not be empty");
    Assert.notNull(chargedUnit, "ChargedUnit must not be null");
    Assert.notNull(chargedAmount, "ChargedAmount must not be null");
    Assert.isTrue(chargedAmount.signum() == 1, "Amount must be > 0");
    Assert.notNull(tax, "Tax must not be null");
    Assert.notNull(netPrice, "NetPrice must not be null");

    BigDecimal roundedNetPrice = netPrice.setScale(PRECISSION_DIGITS, ROUNDING_MODE);

    this.description = description;
    this.chargedUnit = chargedUnit;
    this.chargedAmount = chargedAmount;
    this.tax = tax;
    this.netPrice = roundedNetPrice;
    this.grossPrice = calculateGrossPrice(tax, roundedNetPrice);
    this.taxPrice = calculateTaxPrice(tax, roundedNetPrice);
  }

  private static BigDecimal calculateGrossPrice(Tax tax, BigDecimal netPrice) {
    return netPrice.add(tax.calcTaxOfNetPrice(netPrice));
  }

  private static BigDecimal calculateTaxPrice(Tax tax, BigDecimal netPrice) {
    return tax.calcTaxOfNetPrice(netPrice);
  }

  @SuppressWarnings("unused")
  private Item() {
    //Default constructor for Hibernate
  }

  public String getDescription() {
    return description;
  }

  public Unit getChargedUnit() {
    return chargedUnit;
  }

  public BigInteger getChargedAmount() {
    return chargedAmount;
  }

  public Tax getTax() {
    return tax;
  }

  public BigDecimal getTaxPrice() {
    return taxPrice;
  }

  public BigDecimal getGrossPrice() {
    return grossPrice;
  }

  public BigDecimal getNetPrice() {
    return netPrice;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder()
        .append(description)
        .append(chargedUnit)
        .append(chargedAmount)
        .append(tax)
        .append(netPrice)
        .hashCode();
  }

  @Override
  public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
    Item other = (Item) obj;

    return new EqualsBuilder()
        .append(this.description, other.description)
        .append(this.chargedUnit, other.chargedUnit)
        .append(this.chargedAmount, other.chargedAmount)
        .append(this.tax, other.tax)
        .append(this.netPrice, other.netPrice)
        .isEquals();
  }

  @Override
  public String toString() {
    return "Item [" + description + " | " + chargedAmount + "/" + chargedUnit
        + " | taxPrice=" + taxPrice + " (" + tax + ") | grossPrice=" + grossPrice + " | netPrice="
        + netPrice + "]";
  }

}
