package at.willinger.invoicer.domain.model.invoice.exception;

import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.shared.BaseException;

@SuppressWarnings("serial")
public class InvoiceIdNotFoundException extends BaseException {

	private final InvoiceId invoiceId;

	public InvoiceIdNotFoundException(InvoiceId invoiceId) {
		super(renderMessage(invoiceId));
		this.invoiceId = invoiceId;
	}
	
	private static String renderMessage(InvoiceId invoiceTypeId) {
		return String.format("No Invoice found for id %s", invoiceTypeId);
	}

	public InvoiceId getInvoiceId() {
		return invoiceId;
	}

	@Override
	public String getErrorCode() {
		return "INVOICE_ID_NOT_FOUND";
	}

}
