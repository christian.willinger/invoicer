package at.willinger.invoicer.domain.model.invoicetype;

import java.util.List;
import java.util.Optional;

public interface InvoiceTypeRepository {

  /**
   * Stores a given {@link InvoiceType}.
   *
   * @param type Entity to persist.
   */
  void persist(InvoiceType type);

  /**
   * Loads an {@link InvoiceType} by its ID.
   *
   * @param id ID of the {@link InvoiceType}.
   * @return Optional with loaded entity, or empty optional if no type was found by the given id.
   */
  Optional<InvoiceType> findAndLockById(InvoiceTypeId id);

  Optional<InvoiceType> findById(InvoiceTypeId invoiceTypeId);

  /**
   * Loads all persisted {@link InvoiceType}s.
   *
   * @return All types.
   */
  List<InvoiceType> search();

}
