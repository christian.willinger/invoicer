package at.willinger.invoicer.domain.model.invoicetype.exception;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.shared.BaseException;

@SuppressWarnings("serial")
public class InvoiceTypeIdConflictException extends BaseException {

	private final InvoiceTypeId invoiceTypeId;
	
	public InvoiceTypeIdConflictException(InvoiceTypeId invoiceTypeId) {
		super(renderMessage(invoiceTypeId));
		this.invoiceTypeId = invoiceTypeId;
	}
	
	private static String renderMessage(InvoiceTypeId invoiceTypeId) {
		return String.format("InvoiceTypeId [%s] is already used by another invoice type",
				invoiceTypeId);
	}

	public InvoiceTypeId getInvoiceTypeId() {
		return invoiceTypeId;
	}

	@Override
	public String getErrorCode() {
		return "INVOICE_TYPE_ID_CONFLICT";
	}

}
