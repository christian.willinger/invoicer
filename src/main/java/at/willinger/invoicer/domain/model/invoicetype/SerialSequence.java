package at.willinger.invoicer.domain.model.invoicetype;

import at.willinger.invoicer.domain.model.invoicetype.exception.EndOfSerialSequenceException;
import at.willinger.invoicer.domain.model.invoice.Serial;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.Assert;

/**
 * Sequence with range for serial number.
 *
 * @author Christian Willinger
 *
 */
@Embeddable
public class SerialSequence {

  @Column(name = "serial_sequence_start")
  private Long start;

  @Column(name = "serial_sequence_end")
  private Long end;

  @Column(name = "serial_sequence_value")
  private Long value;

  public SerialSequence(Long start, Long end) {
    Assert.notNull(start, "Start value must not be null");
    Assert.notNull(end, "End value must not be null");

    this.start = start;
    this.end = end;

    swapStartEndIfNeeded();

    this.value = this.start;
  }

  private SerialSequence(Long start, Long end, Long value) {
    if(value > end) {
      throw new EndOfSerialSequenceException();
    }

    this.start = start;
    this.end = end;
    this.value = value;
  }

  @SuppressWarnings("unused")
  private SerialSequence() {
    //Default constructor for hibernate
  }

  private void swapStartEndIfNeeded() {
    if(start > end) {
      Long tmp = start;
      start = end;
      end = tmp;
    }
  }

  public SerialSequence increment()
      throws EndOfSerialSequenceException {
    long newValue = value + 1;
    return new SerialSequence(start, end, newValue);
  }

  public Serial getSerial() {
    return new Serial(value);
  }

  public Long getStart() {
    return start;
  }

  public Long getEnd() {
    return end;
  }

  public Long getValue() {
    return value;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder()
        .append(start)
        .append(end)
        .append(value)
        .toHashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    SerialSequence other = (SerialSequence) obj;
    return new EqualsBuilder()
        .append(this.start, other.start)
        .append(this.end, other.end)
        .append(this.value, other.value)
        .isEquals();
  }

  @Override
  public String toString() {
    return "SerialSequence [Range: [" + start + "-" + end + "], value: " + value + "]";
  }

}
