package at.willinger.invoicer.domain.model.invoice.exception;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoice.InvoiceStatus;
import at.willinger.invoicer.domain.model.shared.BaseException;
import java.util.Set;
import org.springframework.util.StringUtils;

@SuppressWarnings("serial")
public class InvalidInvoiceStatusException extends BaseException {
	
	private final InvoiceId invoiceId;
	
	private final InvoiceStatus actualStatus;
	
	private final Set<InvoiceStatus> expectedStatuses;

	public InvalidInvoiceStatusException(Invoice invoice, Set<InvoiceStatus> expectedStatuses) {
		super(renderMessage(invoice, expectedStatuses));
		this.invoiceId = invoice.getId();
		this.actualStatus = invoice.getStatus();
		this.expectedStatuses = expectedStatuses;
	}
	
	private static String renderMessage(Invoice invoice, Set<InvoiceStatus> expectedStatuses) {
		return String.format("Invoice [%s] is in status [%s] but allowed is [%s]",
				invoice.getId(),
				invoice.getStatus(),
				StringUtils.collectionToCommaDelimitedString(expectedStatuses));
	}

	public InvoiceId getInvoiceId() {
		return invoiceId;
	}

	public InvoiceStatus getActualStatus() {
		return actualStatus;
	}

	public Set<InvoiceStatus> getExpectedStatuses() {
		return expectedStatuses;
	}

	@Override
	public String getErrorCode() {
		return "INVALID_INVOICE_STATUS";
	}
	
}
