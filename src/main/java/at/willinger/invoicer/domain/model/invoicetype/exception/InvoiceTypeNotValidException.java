package at.willinger.invoicer.domain.model.invoicetype.exception;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.TypeValidationError;
import at.willinger.invoicer.domain.model.shared.BaseException;
import java.util.Collections;
import java.util.Set;

@SuppressWarnings("serial")
public class InvoiceTypeNotValidException extends BaseException {

	private final InvoiceTypeId invoiceTypeId;
	private final Set<TypeValidationError> errors;

	public InvoiceTypeNotValidException(InvoiceType invoiceType, Set<TypeValidationError> errors) {
		super(renderMessage(invoiceType, errors));

		this.invoiceTypeId = invoiceType.getId();
		this.errors = errors;
	}
	
	private static String renderMessage(InvoiceType invoiceType, Set<TypeValidationError> errors) {
		return String.format("%s has errors %s",
				invoiceType, errors);
	}

	public InvoiceTypeId getInvoiceTypeId() {
		return invoiceTypeId;
	}

	public Set<TypeValidationError> getErrors() {
		return Collections.unmodifiableSet(errors);
	}

	@Override
	public String getErrorCode() {
		return "INVOICE_TYPE_NOT_VALID";
	}
	
}
