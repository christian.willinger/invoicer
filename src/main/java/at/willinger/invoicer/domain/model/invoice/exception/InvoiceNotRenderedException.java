package at.willinger.invoicer.domain.model.invoice.exception;

import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.shared.BaseException;

@SuppressWarnings("serial")
public class InvoiceNotRenderedException  extends BaseException {
	
	private final InvoiceId invoiceId;
	
	public InvoiceNotRenderedException(InvoiceId invoiceId) {
		super(renderMessage(invoiceId));
		this.invoiceId = invoiceId;
	}
	
	private static String renderMessage(InvoiceId invoiceId) {
		return String.format("Invoice [%s] is not rendered", invoiceId);
	}

	public InvoiceId getInvoiceId() {
		return invoiceId;
	}

	@Override
	public String getErrorCode() {
		return "INVOICE_NOT_RENDERED";
	}

}
