package at.willinger.invoicer.domain.model.invoice.types;

import static at.willinger.invoicer.domain.model.invoice.InvoiceStatus.CANCELED;
import static at.willinger.invoicer.domain.model.invoice.InvoiceStatus.FINALIZED;

import at.willinger.invoicer.domain.model.invoice.Address;
import at.willinger.invoicer.domain.model.invoice.CustomerId;
import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import org.springframework.util.Assert;

/**
 * Represents the default invoice case.
 * The standard invoice can be canceled.
 *
 * @author Christian Willinger
 */
public class StandardInvoice extends Invoice {

  private InvoiceId cancellationInvoiceId;

  public StandardInvoice(
      InvoiceId id, CustomerId customerId, InvoiceType invoiceType, Address address) {
    super(id, customerId, invoiceType, address);
  }

  public void cancel(InvoiceId cancellationInvoiceId) {
    Assert.notNull(cancellationInvoiceId, "CancellationInvoiceId must not be null");
    validateCancellationInvoiceIdIsNoLoop(cancellationInvoiceId);
    validateStatus(FINALIZED);

    this.cancellationInvoiceId = cancellationInvoiceId;
    this.status = CANCELED;
  }

  private void validateCancellationInvoiceIdIsNoLoop(InvoiceId cancellationInvoiceId) {
    if(getId().equals(cancellationInvoiceId)) {
      throw new IllegalArgumentException("CancellationInvoiceId and Id must not be equal");
    }
  }

  public InvoiceId getCancellationInvoiceId() {
    return cancellationInvoiceId;
  }

}
