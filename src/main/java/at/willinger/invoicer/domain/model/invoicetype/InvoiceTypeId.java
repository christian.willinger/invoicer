package at.willinger.invoicer.domain.model.invoicetype;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Uniquely identifies an {@link InvoiceType}.
 * Allows 1-60 alphanumeric chars and .-# as separators.
 *
 * @author Christian Willinger
 *
 */
@Embeddable
public class InvoiceTypeId implements Serializable {

  private static final long serialVersionUID = 4921510877755701844L;

  private static final Pattern PATTERN = Pattern.compile("[a-z A-Z 0-9 \\. \\_ #]{1,60}");

  @Column(name = "invoice_type_id")
  private String invoiceTypeId;

  public InvoiceTypeId(String invoiceTypeId) {
    validate(invoiceTypeId);
    this.invoiceTypeId = invoiceTypeId;
  }

  private void validate(String invoiceTypeId) {
    if(!isValid(invoiceTypeId)) {
      throw new IllegalArgumentException("InvoiceTypeId only allows alphanumeric characters and .-#");
    }
  }

  public static boolean isValid(String invoiceTypeId) {
    return Objects.nonNull(invoiceTypeId)
      && PATTERN.matcher(invoiceTypeId).matches();
  }

  @SuppressWarnings("unused")
  private InvoiceTypeId() {
    //Default constructor for Hiberante
  }

  public String getValue() {
    return invoiceTypeId;
  }

  @Override
  public String toString() {
    return invoiceTypeId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    InvoiceTypeId that = (InvoiceTypeId) o;

    return new EqualsBuilder()
        .append(invoiceTypeId, that.invoiceTypeId)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder()
        .append(invoiceTypeId)
        .toHashCode();
  }

}
