package at.willinger.invoicer.domain.model.invoice.types;

import at.willinger.invoicer.domain.model.invoice.Address;
import at.willinger.invoicer.domain.model.invoice.CustomerId;
import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import org.springframework.util.Assert;

/**
 * Cancels an other invoice.
 *
 * @author Christian Willinger
 */
public class CancellationInvoice extends Invoice {

  @AttributeOverride(name = "invoiceId", column = @Column(name = "canceled_invoice_id"))
  private InvoiceId canceledInvoiceId;

  public CancellationInvoice(
      InvoiceId id, CustomerId customerId, InvoiceType invoiceType, Address address,
      InvoiceId canceledInvoiceId) {
    super(id, customerId, invoiceType, address);
    setCanceledInvoiceId(canceledInvoiceId);
  }

  private void setCanceledInvoiceId(InvoiceId canceledInvoiceId) {
    Assert.notNull(canceledInvoiceId, "CanceledInvoiceId must not be null");
    validateCanceledInvoiceIdIsNoLoop(canceledInvoiceId);

    this.canceledInvoiceId = canceledInvoiceId;
  }

  private void validateCanceledInvoiceIdIsNoLoop(InvoiceId canceledInvoiceId) {
    if(getId().equals(canceledInvoiceId)) {
      throw new IllegalArgumentException("InvoiceId and CanceledInvoiceId must not be equal");
    }
  }

  public InvoiceId getCanceledInvoiceId() {
    return canceledInvoiceId;
  }

}
