package at.willinger.invoicer.domain.model.invoicetype.exception;

import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.shared.BaseException;

/**
 * Is thrown if an InvoiceType can not be found by a given ID.
 * 
 * @author Christian Willinger
 *
 */
@SuppressWarnings("serial")
public class InvoiceTypeIdNotFoundExcpeption extends BaseException {
	
	private final InvoiceTypeId invoiceTypeId;

	public InvoiceTypeIdNotFoundExcpeption(InvoiceTypeId invoiceTypeId) {
		super(renderMessage(invoiceTypeId));
		this.invoiceTypeId = invoiceTypeId;
	}
	
	private static String renderMessage(InvoiceTypeId invoiceTypeId) {
		return String.format("No InvoiceType found for id %s", invoiceTypeId);
	}

	public InvoiceTypeId getInvoiceTypeId() {
		return invoiceTypeId;
	}

	@Override
	public String getErrorCode() {
		return "INVOICE_TYPE_ID_NOT_FOUND";
	}

}
