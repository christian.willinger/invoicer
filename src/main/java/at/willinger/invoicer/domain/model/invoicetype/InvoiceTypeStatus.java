package at.willinger.invoicer.domain.model.invoicetype;

/**
 * Describes the lifecycle status of an invoice type.
 *
 * @author Christian Willinger
 *
 */
public enum InvoiceTypeStatus {

  /**
   * The Type is created but can not be used for invoicing.
   * In this status the type is allowed to not have a template file assigned.
   */
  NEW,

  /**
   * Type can be used for invoicing.
   */
  ACTIVE,

  /**
   * Type and Templates cannot be modified anymore.
   */
  INACTIVE,

}
