package at.willinger.invoicer.domain.model.invoice;

import java.util.regex.Pattern;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.Assert;

/**
 * Represents a invoice address as printed on the invoice.
 * 
 * Each line allows 40 alphanumeric characters as well a .,'&/- and spaces.
 * Spaces in the beginning and end will be removed.
 * 
 * @author Christian Willinger
 *
 */
@Embeddable
public class Address {
	
	private static final Pattern VALIDITY_PATTERN = Pattern
			.compile("[a-z A-Z 0-9 . , ' & / \\-]{0,40}");
	
	@Column(name = "address_line_1")
	private String line1;
	
	@Column(name = "address_line_2")
	private String line2;
	
	@Column(name = "address_line_3")
	private String line3;
	
	@Column(name = "address_line_4")
	private String line4;
	
	@Column(name = "address_line_5")
	private String line5;

	public Address(String line1, String line2, String line3, String line4, String line5) {
		Assert.notNull(line1, "Address line 1 must not be null");
		Assert.notNull(line2, "Address line 2 must not be null");
		Assert.notNull(line3, "Address line 3 must not be null");
		Assert.notNull(line4, "Address line 4 must not be null");
		Assert.notNull(line5, "Address line 5 must not be null");
		
		this.line1 = normalize(line1);
		this.line2 = normalize(line2);
		this.line3 = normalize(line3);
		this.line4 = normalize(line4);
		this.line5 = normalize(line5);
		
		validateLine(this.line1, 1);
		validateLine(this.line2, 2);
		validateLine(this.line3, 3);
		validateLine(this.line4, 4);
		validateLine(this.line5, 5);
	}
	
	private static void validateLine(String lineString, int lineNumber) {
		if(!isValidLine(lineString)) {
			String template = "Address line %d contains illegal character or length. "
							+ "Only 40 alpanumeric chars and .,'&/- are allowed."; 
			String msg = String.format(template, lineNumber);
			throw new IllegalArgumentException(msg);
		}
	}

	public static boolean isValidLine(String line) {
		return VALIDITY_PATTERN.matcher(line).matches();
	}
	
	private static String normalize(String line) {
		return line.trim();
	}
	
	@SuppressWarnings("unused")
	private Address() {
		//Default constructor for Hibernate
	}

	public String getLine1() {
		return line1;
	}

	public String getLine2() {
		return line2;
	}

	public String getLine3() {
		return line3;
	}

	public String getLine4() {
		return line4;
	}
	
	public String getLine5() {
		return line5;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(line1)
				.append(line2)
				.append(line3)
				.append(line4)
				.append(line5)
				.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		
		return new EqualsBuilder()
				.append(line1, other.line1)
				.append(line2, other.line2)
				.append(line3, other.line3)
				.append(line4, other.line4)
				.append(line5, other.line5)
				.isEquals();
	}

	@Override
	public String toString() {
		return "Address [" + line1 + ", " + line2 + ", " + line3 + ", " + line4 + ", " + line5 + "]";
	}
	
}
