package at.willinger.invoicer.domain.model.invoice;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Pattern;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ID of an {@link Invoice}.
 * Allows 1-60 alphanumeric chars and .-# as separators.
 *
 * @author Christian Willinger
 *
 */
@Embeddable
public class InvoiceId implements Serializable {
	
	private static final long serialVersionUID = -2706134799807817038L;

	private static final Pattern PATTERN = Pattern.compile("[a-z A-Z 0-9 \\. \\_ #]{1,60}");

	@Column(name = "invoice_id")
	private String invoiceId;
	
	public InvoiceId(String invoiceId) {
		validate(invoiceId);
		this.invoiceId = invoiceId;
	}

	private void validate(String invoiceId) {
		if(!isValid(invoiceId)) {
			throw new IllegalArgumentException("InvoiceId only allows alphanumeric characters and .-#");
		}
	}

	public static boolean isValid(String invoiceId) {
		return Objects.nonNull(invoiceId)
				&& PATTERN.matcher(invoiceId).matches();
	}

	public String getValue() {
		return invoiceId;
	}

	@Override
	public String toString() {
		return invoiceId;
	}

	@Override
	public int hashCode() {
		return invoiceId.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceId other = (InvoiceId) obj;
		return other.invoiceId.equals(invoiceId);
	}

}
