package at.willinger.invoicer.domain.model.invoice;

import static at.willinger.invoicer.domain.model.invoice.InvoiceStatus.FINALIZED;
import static at.willinger.invoicer.domain.model.invoice.InvoiceStatus.NEW;

import at.willinger.invoicer.domain.model.event.DomainEvent;
import at.willinger.invoicer.domain.model.event.InvoiceCreatedEvent;
import at.willinger.invoicer.domain.model.invoice.exception.InvalidInvoiceStatusException;
import at.willinger.invoicer.domain.model.invoice.item.Item;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Transient;
import org.springframework.data.domain.DomainEvents;
import org.springframework.util.Assert;

/**
 * Invoice is the central class in the invoice model.
 * It is the root of the invoice aggregate.
 * 
 * It consists of items together with the sum of all items.
 * 
 * @author Christian Willinger
 *
 */
@Entity
public abstract class Invoice {
	
	@EmbeddedId
	private InvoiceId id;
	
	@Embedded
	private CustomerId customerId;
	
	@ManyToOne
	@JoinColumn(name = "invoice_type_id")
	private InvoiceType invoiceType;
	
	@Embedded
	private Address address;
	
	@Embedded
	private Serial serial;
	
	private LocalDateTime invoiceDate;
	
	@Enumerated(EnumType.STRING)
	protected InvoiceStatus status = NEW;
	
	private Boolean rendered = Boolean.FALSE;

	@OneToMany(cascade = CascadeType.ALL)
	@OrderColumn(name = "invoice_position")
	@JoinColumn(name = "invoice_id")
	private List<Item> items = new ArrayList<>();
	
	private BigDecimal taxSum = BigDecimal.ZERO;
	
	private BigDecimal grossSum = BigDecimal.ZERO;
	
	private BigDecimal netSum = BigDecimal.ZERO;
	
	@Transient
	private List<DomainEvent> domainEvents = new ArrayList<>();

	public Invoice(InvoiceId id, CustomerId customerId, InvoiceType invoiceType, Address address) {
		Assert.notNull(id, "InvoiceId must not be null");
		Assert.notNull(customerId, "CustomerId must not be null");
		Assert.notNull(invoiceType, "InvoiceType must not be null");
		Assert.notNull(address, "Address must not be null");

		this.id = id;
		this.customerId = customerId;
		this.invoiceType = invoiceType;
		this.address = address;

		addDomainEvent(
				new InvoiceCreatedEvent(id));
	}
	
	@SuppressWarnings("unused")
	private Invoice() {
		//Default Constructor for Hibernate
	}
	
	public void addItem(Item item) 
			throws InvalidInvoiceStatusException {
		Assert.notNull(item, "Item must not be null");
		validateStatus(NEW);
		
		items.add(item);
		
		grossSum = calcGrossSum();
		netSum = calcNetSum();
		taxSum = calcTaxSum();
	}
	
	private BigDecimal calcGrossSum() {
		return items.stream()
				.map(Item::getGrossPrice)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}
	
	private BigDecimal calcNetSum() {
		return items.stream()
				.map(Item::getNetPrice)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}
	
	private BigDecimal calcTaxSum() {
		return items.stream()
				.map(Item::getTaxPrice)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public void finalizeWithSerial(Serial serial)
			throws InvalidInvoiceStatusException {
		Assert.notNull(serial, "Serial must not be null");
		validateStatus(NEW);
		
		this.status = FINALIZED;
		this.invoiceDate = LocalDateTime.now();
		this.serial = serial;
	}
	
	public void render() {
		validateStatus(FINALIZED);
		
		this.rendered = Boolean.TRUE;
	}

	protected void validateStatus(InvoiceStatus s1, InvoiceStatus... s2) {
		Set<InvoiceStatus> allowedStatus = EnumSet.of(s1, s2);

		if(!allowedStatus.contains(status)) {
			throw new InvalidInvoiceStatusException(this, allowedStatus);
		}
	}
	
	private void addDomainEvent(DomainEvent event) {
		domainEvents.add(event);
	}

	public InvoiceId getId() {
		return id;
	}

	public CustomerId getCustomerId() {
		return customerId;
	}

	public InvoiceType getInvoiceType() {
		return invoiceType;
	}

	public Address getAddress() {
		return address;
	}

	public Serial getSerial() {
		return serial;
	}

	public LocalDateTime getInvoiceDate() {
		return invoiceDate;
	}

	public InvoiceStatus getStatus() {
		return status;
	}
	
	public Boolean getRendered() {
		return rendered;
	}

	public List<Item> getItems() {
		return Collections.unmodifiableList(items);
	}

	public BigDecimal getTaxSum() {
		return taxSum;
	}

	public BigDecimal getGrossSum() {
		return grossSum;
	}

	public BigDecimal getNetSum() {
		return netSum;
	}

	@DomainEvents
	public List<DomainEvent> getDomainEvents() {
		return domainEvents;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Invoice other = (Invoice) obj;
		return id.equals(other.id);
	}
	
	@Override
	public String toString() {
		return "Invoice [" + id + "]";
	}

	public String toStatusString() {
		return "Invoice [status=" + status + ", rendered=" + rendered + "]";
	}
	
}
