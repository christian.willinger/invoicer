package at.willinger.invoicer.domain.model.invoicetype;

import at.willinger.invoicer.domain.model.invoicetype.exception.InvalidInvoiceTypeStatusException;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeNotValidException;
import java.util.EnumSet;
import java.util.Set;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * Represents a certain type of Invoices in the billing process.
 * A type can be an order, booking, etc.
 * Each type has its own template and its own serial range for the account keeping.
 *
 * @author Christian Willinger
 *
 */
@Entity
public class InvoiceType {

  @EmbeddedId
  private InvoiceTypeId id;

  private InvoiceTypeStatus status = InvoiceTypeStatus.NEW;

  @Embedded
  private SerialSequence serialSequence;

  private String templateFileName;

  private String cancellationTemplateFileName;

  public InvoiceType(InvoiceTypeId id, SerialSequence serialSequence) {
    Assert.notNull(id, "InvoiceTypeId must not be null");
    Assert.notNull(serialSequence, "SerialSequence must not be null");

    this.id = id;
    this.serialSequence = serialSequence;
  }

  @SuppressWarnings("unused")
  private InvoiceType() {
    //Default Constructor for Hibernate
  }

  public void setTemplateFileName(String fileName) {
    Assert.hasText(fileName, "FileName must not be empty");
    validateStatus(InvoiceTypeStatus.NEW);

    this.templateFileName = fileName;
  }

  public void setCancellationTemplateFileName(String fileName) {
    Assert.hasText(fileName, "FileName must not be empty");
    validateStatus(InvoiceTypeStatus.NEW);

    this.cancellationTemplateFileName = fileName;
  }

  public void activate()
      throws InvalidInvoiceTypeStatusException, InvoiceTypeNotValidException {
    validateStatus(InvoiceTypeStatus.NEW);
    validateFields();

    this.status = InvoiceTypeStatus.ACTIVE;
  }

  private void validateFields() {
    Set<TypeValidationError> errors = TypeValidator.validate(this);
    if(!errors.isEmpty()) {
      throw new InvoiceTypeNotValidException(this, errors);
    }
  }

  public void deactivateInvoiceType() {
    validateStatuses(EnumSet.of(InvoiceTypeStatus.NEW, InvoiceTypeStatus.ACTIVE));

    status = InvoiceTypeStatus.INACTIVE;
  }

  private void validateStatus(InvoiceTypeStatus allowedStatus) {
    validateStatuses(EnumSet.of(allowedStatus));
  }

  public void validateStatuses(Set<InvoiceTypeStatus> allowedStatus) {
    if(!allowedStatus.contains(status)) {
      throw new InvalidInvoiceTypeStatusException(this, allowedStatus);
    }
  }

  public boolean hasTemplateFile() {
    return StringUtils.hasText(templateFileName);
  }

  public boolean hasCancellationTemplateFile() {
    return StringUtils.hasText(cancellationTemplateFileName);
  }

  public InvoiceTypeId getId() {
    return id;
  }

  public void incrementSerial() {
    serialSequence = serialSequence.increment();
  }

  public InvoiceTypeStatus getStatus() {
    return status;
  }

  public SerialSequence getSerialSequence() {
    return serialSequence;
  }

  public String getTemplateFileName() {
    return templateFileName;
  }

  public String getCancellationTemplateFileName() {
    return cancellationTemplateFileName;
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    InvoiceType other = (InvoiceType) obj;
    return id.equals(other.id);
  }

  @Override
  public String toString() {
    return "InvoiceType [" + id + "]";
  }

}
