package at.willinger.invoicer.domain.model.invoice.exception;

import at.willinger.invoicer.domain.model.invoice.InvoiceId;
import at.willinger.invoicer.domain.model.shared.BaseException;

@SuppressWarnings("serial")
public class InvoiceIdConflictException extends BaseException {
	
	private final InvoiceId invoiceId;
	
	public InvoiceIdConflictException(InvoiceId invoiceId) {
		super(renderMessage(invoiceId));
		this.invoiceId = invoiceId;
	}
	
	private static String renderMessage(InvoiceId invoiceId) {
		return String.format("InvoiceId [%s] is already used by another invoice",
				invoiceId);
	}

	public InvoiceId getInvoiceId() {
		return invoiceId;
	}

	@Override
	public String getErrorCode() {
		return "INVOICE_ID_CONFLICT";
	}
	
}