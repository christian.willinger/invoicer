package at.willinger.invoicer.domain.model.invoicetype;

import java.util.HashSet;
import java.util.Set;

/**
 * Validates a {@link InvoiceType} to be complete for activation.
 *
 * @author Christian Willinger
 */
public class TypeValidator {

	private TypeValidator() {
	}
	
	public static Set<TypeValidationError> validate(InvoiceType template) {
		Set<TypeValidationError> errors = new HashSet<>();
		
		if(template.getTemplateFileName() == null) {
			errors.add(TypeValidationError.MISSING_TEMPLATE);
		}
		
		if(template.getCancellationTemplateFileName() == null) {
			errors.add(TypeValidationError.MISSING_STORNO_TEMPLATE);
		}
		
		return errors;
	}

}
