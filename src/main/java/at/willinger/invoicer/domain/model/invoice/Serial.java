package at.willinger.invoicer.domain.model.invoice;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.Assert;

@Embeddable
public class Serial {
	
	@Column(name = "serial")
	private Long serialValue;

	public Serial(Long value) {
		Assert.notNull(value, "Serial value must not be null");
		if(value < 1) throw new IllegalArgumentException("Serial value must be > 0");
		this.serialValue = value;
	}
	
	@SuppressWarnings("unused")
	private Serial() {
		//Default constructor for Hiberante
	}

	public Long getValue() {
		return serialValue;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Serial serial = (Serial) o;

		return new EqualsBuilder()
				.append(serialValue, serial.serialValue)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(serialValue)
				.toHashCode();
	}

	@Override
	public String toString() {
		return serialValue.toString();
	}
	
}
