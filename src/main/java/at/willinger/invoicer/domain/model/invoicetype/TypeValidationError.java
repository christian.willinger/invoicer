package at.willinger.invoicer.domain.model.invoicetype;

public enum TypeValidationError {

  MISSING_TEMPLATE,

  MISSING_STORNO_TEMPLATE,

}
