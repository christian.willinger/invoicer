package at.willinger.invoicer.domain.model.invoice;

import at.willinger.invoicer.domain.model.invoice.item.Item;
import at.willinger.invoicer.domain.model.invoice.types.StandardInvoice;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeId;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeRepository;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceTypeStatus;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvalidInvoiceTypeStatusException;
import at.willinger.invoicer.domain.model.invoicetype.exception.InvoiceTypeIdNotFoundExcpeption;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates invoices.
 *
 * @author Christian Willinger
 */
public class InvoiceFactory {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  private InvoiceTypeRepository invoiceTypeRepository;

  public InvoiceFactory(InvoiceTypeRepository invoiceTypeRepository) {
    this.invoiceTypeRepository = invoiceTypeRepository;
  }

  /**
   * @param invoiceTypeId Id of the {@link InvoiceType} assigned to the invoice.
   * @param id            Id of the new {@link Invoice}.
   * @param customerId    Id of the Customer which is invoiced.
   * @param address       Address of the {@link Invoice}.
   * @param items         Line items to be added.
   * @return Created invoice.
   * @throws InvoiceTypeIdNotFoundExcpeption   If no {@link InvoiceType} found for the given {@link
   *                                           InvoiceTypeId}.
   * @throws InvalidInvoiceTypeStatusException If the used {@link InvoiceType} is not {@link
   *                                           InvoiceTypeStatus#ACTIVE}
   */
  public StandardInvoice create(
      InvoiceTypeId invoiceTypeId, InvoiceId id, CustomerId customerId, Address address,
      List<Item> items)
      throws InvoiceTypeIdNotFoundExcpeption, InvalidInvoiceTypeStatusException {
    logger.debug("Assemble new Invoice");

    InvoiceType invoiceType = findInvoiceType(invoiceTypeId);

    StandardInvoice invoice = new StandardInvoice(id, customerId, invoiceType, address);

    for (Item item : items) {
      invoice.addItem(item);
      logger.debug("{} add {}", invoice, item);
    }

    logger.debug("New {} assembled", invoice);

    return invoice;
  }

  private InvoiceType findInvoiceType(InvoiceTypeId invoiceTypeId) {
    return invoiceTypeRepository
        .findById(invoiceTypeId)
        .orElseThrow(() -> new InvoiceTypeIdNotFoundExcpeption(invoiceTypeId));
  }

}
