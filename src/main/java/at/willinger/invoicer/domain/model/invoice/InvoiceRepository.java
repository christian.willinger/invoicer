package at.willinger.invoicer.domain.model.invoice;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface InvoiceRepository {
	
	/**
	 * Saves an invoice.
	 * 
	 * @param invoice Entity to persist.
	 * @return Persisted entity.
	 */
	Invoice persist(Invoice invoice);
	
	/**
	 * @param id ID of the Invoice
	 * @return Found entity
	 */
	Optional<Invoice> findById(InvoiceId id);
	
	/**
	 * @param id ID of the Invoice
	 * @return Found entity
	 */
	Optional<Invoice> findAndLockById(InvoiceId id);

	/**
	 * @param spec Specification to search invoices.
	 * @param pageRequest Pagingation to limit search result.
	 * @return Found entities.
	 */
	Page<Invoice> findAll(InvoiceSearchSpecification spec, PageRequest pageRequest);
}
