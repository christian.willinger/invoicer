package at.willinger.invoicer.domain.model.invoice;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.springframework.util.Assert;

@Embeddable
public class CustomerId {

	@Column(name = "customer_id")
	private String value;
	
	public CustomerId(String value) {
		Assert.hasText(value, "CustomerId value must not be null");
		this.value = value;
	}
	
	protected CustomerId() {
		//Default constructor for Hiberante
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerId other = (CustomerId) obj;
		return other.value.equals(value);
	}
	
}
