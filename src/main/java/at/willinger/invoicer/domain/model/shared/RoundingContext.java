package at.willinger.invoicer.domain.model.shared;

import java.math.RoundingMode;

public class RoundingContext {

	private RoundingContext() {
	}
	
	public static final int PRECISSION_DIGITS = 2;
	
	public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_DOWN;

}
