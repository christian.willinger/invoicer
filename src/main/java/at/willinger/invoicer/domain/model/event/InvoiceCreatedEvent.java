package at.willinger.invoicer.domain.model.event;

import at.willinger.invoicer.domain.model.invoice.InvoiceId;

public class InvoiceCreatedEvent implements DomainEvent {
	
	private InvoiceId invoiceId;
	
	public InvoiceCreatedEvent(InvoiceId invoiceId) {
		this.invoiceId = invoiceId;
	}

	public InvoiceId getInvoiceId() {
		return invoiceId;
	}

	@Override
	public String toString() {
		return "InvoiceCreatedEvent [invoiceId=" + invoiceId + "]";
	}

}
