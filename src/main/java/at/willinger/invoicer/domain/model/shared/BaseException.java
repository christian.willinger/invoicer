package at.willinger.invoicer.domain.model.shared;

@SuppressWarnings("serial")
public abstract class BaseException extends RuntimeException {

	public BaseException() {
		super();
	}

	public BaseException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public BaseException(String msg) {
		super(msg);
	}
	
	public abstract String getErrorCode();

}
