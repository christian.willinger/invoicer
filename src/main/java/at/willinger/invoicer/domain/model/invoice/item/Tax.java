package at.willinger.invoicer.domain.model.invoice.item;

import static at.willinger.invoicer.domain.model.shared.RoundingContext.PRECISSION_DIGITS;
import static at.willinger.invoicer.domain.model.shared.RoundingContext.ROUNDING_MODE;

import java.math.BigDecimal;
import javax.persistence.Embeddable;
import org.springframework.util.Assert;

/**
 * Represents the tax defined on an item in percent.
 * 
 * @author Christian Willinger
 *
 */
@Embeddable
public class Tax {
	
	private BigDecimal taxInPercent;

	private Tax(BigDecimal valueInPercent) {
		Assert.notNull(valueInPercent, "Tax value must not be null");
		Assert.isTrue(valueInPercent.signum() != -1, "Tax must not be negative");
		
		this.taxInPercent = valueInPercent.setScale(PRECISSION_DIGITS, ROUNDING_MODE);
	}

	@SuppressWarnings("unused")
	private Tax() {
		//Default Constructor for Hibernate
	}
	
	public static Tax ofPercent(BigDecimal valueInPercent) {
		return new Tax(valueInPercent);
	}
	
	public BigDecimal calcTaxOfNetPrice(BigDecimal netPrice) {
		return netPrice
				.divide(BigDecimal.valueOf(100))
				.multiply(taxInPercent)
				.setScale(PRECISSION_DIGITS, ROUNDING_MODE);
	}

	public BigDecimal getValueInPercent() {
		return taxInPercent;
	}

	public static int getPrecissionDigits() {
		return PRECISSION_DIGITS;
	}

	@Override
	public int hashCode() {
		return taxInPercent.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tax other = (Tax) obj;
			return taxInPercent.equals(other.taxInPercent);
	}

	@Override
	public String toString() {
		return taxInPercent + "%";
	}
	
}
