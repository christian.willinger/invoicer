package at.willinger.invoicer.domain.service;

import at.willinger.invoicer.domain.model.invoice.Invoice;

public interface AccountingService {
	
	void passInvoiceToAccounting(Invoice invoice);

}
