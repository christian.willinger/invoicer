package at.willinger.invoicer.domain.service;

import at.willinger.invoicer.domain.model.invoice.Invoice;
import at.willinger.invoicer.domain.model.invoicetype.InvoiceType;
import java.io.InputStream;

public interface FileStorageService {

	/**
	 * Stores the template file for {@link InvoiceType}.
	 *
	 * @param invoiceType Template the file is for.
	 * @param is Content of the template file.
	 */
	void storeInvoiceTemplate(InvoiceType invoiceType, InputStream is);

	/**
	 * Loads the template file for a {@link InvoiceType}.
	 *
	 * @param invoiceType Template to load template file for.
	 * @return Content of the template file.
	 * @throws IllegalStateException
	 * 			If no template file is persisted for the given template.
	 */
	InputStream loadInvoiceTemplate(InvoiceType invoiceType)
			throws IllegalStateException;

	/**
	 * Stores the storno template file for {@link InvoiceType}.
	 *
	 * @param invoiceType Template the file is for.
	 * @param is Content of the storno template file.
	 */
	void storeInvoiceStornoTemplate(InvoiceType invoiceType, InputStream is);

	/**
	 * Loads the storno template file for a {@link InvoiceType}.
	 *
	 * @param invoiceType Template to load template file for.
	 * @return Content of the storno template file.
	 * @throws IllegalStateException
	 * 			If no storno template file is persisted for the given template.
	 */
	InputStream loadInvoiceStornoTemplate(InvoiceType invoiceType)
			throws IllegalStateException;

	/**
	 * Stores the rendered file for an {@link Invoice}.
	 *
	 * @param invoice Invoice to store file for.
	 * @param is Content of the invoice file.
	 */
	void storeInvoiceFile(Invoice invoice, InputStream is);

	/**
	 * Loads the rendered file for an {@link Invoice}.
	 *
	 * @param invoice Invoice to load the rendered file for.
	 * @return Content Content of the invoice file.
	 * @throws IllegalStateException
	 * 			If no file is persisted for the given {@link Invoice}.
	 */
	InputStream loadInvoiceFile(Invoice invoice)
			throws IllegalStateException;

}
